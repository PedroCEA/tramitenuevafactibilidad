<?php 

function getPlantilla($datos){

$plantilla ='<body>
	<img src="img/logoti.png"> 
	<h2 class="text-center">Solicitud de Movimientos de Factibilidades</h2>
	<table id="psup" name="psup" border=0 class="psup">
	<tr>
	<td>
		<table id="info" name="info" border=0 class="info">
			<tr><td><strong>Dirección Divisional de Factibilidades</strong></td></tr>
			<tr><td><strong>Avenida 5 de Febrero No. 35</strong></td></tr>
			<tr><td><strong>Col. Las Campanas, C.P. 76010</strong></td></tr>
			<tr><td><strong>Santiago de Querétaro, Qro.</strong></td></tr>
			<tr><td><strong>Tel. 211-0600 Ext. 1259 y 1220</strong></td></tr>
		</table>
	</td>
	<td>
		<table id="fraccionamiento" name="fraccionamiento" border=1 class="fraccionamiento">
		<tr><td colspan=2 class="titulofraccionamiento">Fraccionamiento</td></tr>
		<tr><td>No. Expediente</td><td><p class="folioc"><?php echo $folio;?></p></td></tr>
		<tr><td>Fecha</td><td>$fecha</td></tr>
		<tr><td>Trámite</td><td>Solicitud de Factibilidad Nueva</td></tr>
		</table>
		</td>
	</tr>
	</table>';
	foreach ($datos as $dato) {
		
	$si_persona=$dato['Personamoral']; if($si_persona==1){$pm="Si";}else{$pm="No";}
	$plantilla.='<table class="DatosGenerales" name="DatosGenerales" id="DatosGenerales" border=1>
		<tr><td colspan=6 class="tituloDatosGenerales">Datos Generales</td></tr>
		<tr><td colspan=2>Nombre o razón social</td><td colspan=4>'.$dato["Nombre"].'</td></tr>
		<tr><td colspan=4>Registro federal de causantes-Persona moral (['.$pm.'])</td><td colspan=2>'.$dato["RFC"].'</td></tr>
		<tr><td>Domicilio</td><td colspan=5></td></tr>
		<tr><td>Colonia</td><td colspan=3></td><td>Código postal</td><td></td></tr>
		<tr><td>Municipio</td><td></td><td>Estado</td><td></td><td>Teléfono(s)</td><td></td></tr>
		
		<tr><td colspan=2>Nombre del Representante Legal</td><td colspan=4></td></tr>
		<tr><td>Domicilio</td><td colspan=5></td></tr>
		<tr><td>Colonia</td><td colspan=3></td><td>Código postal</td><td></td></tr>
		<tr><td>Municipio</td><td></td><td>Estado</td><td></td><td>Teléfono(s)</td><td></td></tr>
	</table>';
	}
	$plantilla.='<table class="DatosPredio" name="DatosPredio" id="DatosPredio" border=1>
		<tr><td colspan=4 class="tituloDatosPredio">Datos del predio</td></tr>
		<tr><td>Nombre del faccionamiento</td><td colspan=3></td></tr>
		<tr><td>Superficie total (m2)</td><td></td><td>Clave catastral</td><td></td></tr>
		<tr><td colspan="2">Cambia nombre fraccionamiento</td><td colspan=2></td></tr>
		<tr><td>Tipo de vivienda</td><td colspan=3></td></tr>
		<tr><td>Domicilio</td><td colspan=3></td></tr>
		<tr><td>Colonia</td><td colspan=3></td></tr>
		<tr><td>Municipio</td><td></td><td>Estado</td><td>Queretaro</td></tr>
	</table>
	
	<table id="pinf" name="pinf" class="pinf">
	<tr>
	<td><table class="DatosDocumento" name="DatosDocumento" id="DatosDocumento" border="1">
	<tr><td colspan=2 class="tituloDatosPredio">Documento</td></tr>
		<tr><td>Tipo Documento</td><td>Entregado</tr>
		<tr><td></td><td>Si</tr>
		<tr><td></td><td>Si</tr>
		<tr><td></td><td>Si</tr>
		<tr><td></td><td>Si</tr>
		</table>
	</td>
	<td>
	<table class="DatosReqAgua" name="DatosReqAgua" id="DatosReqAgua">
		<tr><td colspan=4 class="tituloDatosPredio">Requerimiento de agua</td></tr>
		<tr><td colspan=2>Doméstico</td><td colspan="1"></td></tr>
		<tr><td colspan=2>Comercial</td><td></td></tr>
		<tr><td colspan=2>Industrial</td><td></td></tr>
		<tr><td colspan=2>Otro </td><td></td></tr>
		<tr><td colspan=2>Total de unidades a servir</td><td></td></tr>
	</table> 
	</table>
<div>
	<div class="divuno">
		<p>Atentamente:</p><br>
		<p>_______________________</p>
		<p>Firma del solicitante</p>
	</div>
	
	<div class="divdos">
		<p>Recibí:</p><br>	
		<p>________________________</p>	
	</div>
	
	
	
</div>
	
</body>';

return $plantilla;
}
 
?>
