<?php 
include 'conexion.php';
$fecha=date("Y-m-d");
$folio=$_GET['radnumber'];
$user = $_GET['user'];

$cons = "SELECT * FROM PadronDesarrolladores where RFC = '$user'";
$registro = sqlsrv_query($conn,$cons);
$row = sqlsrv_fetch_array( $registro, SQLSRV_FETCH_ASSOC);
//var_dump($row);

$nomRep = $_GET['nomRep'];
$telRep = $_GET['telRep'];
$correoRep = $_GET['correoRep'];

$nomfracc=$_GET['nomfracc'];
$claveCat=$_GET['claveCat'];
$tipoV=$_GET['tipoV'];
$cp=$_GET['cp'];
$telef=$_GET['telef'];
$correo=$_GET['correo'];
$superficie=$_GET['superficie'];
$regCon=$_GET['regCon'];
$domP=$_GET['domP'];
$colP=$_GET['colP'];
$munP=$_GET['munP'];
$dom=$_GET['dom'];
$com=$_GET['com'];
$ind=$_GET['ind'];
$otr=$_GET['otr'];
$totuni=$_GET['totuni'];

$doc1 = $_GET['doc1'];
$doc2 = $_GET['doc2'];
$doc3 = $_GET['doc3'];
$doc4 = $_GET['doc4'];
$doc5 = $_GET['doc5'];


?>
<html lang="en">
<head runat="server">
	<title>Comisión Estatal de Aguas</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
	 <!--FORMATO DE TABLAS-->
        <link type="text/css" href="css/style.css" rel="stylesheet" />
</head>
<body>
	
	<img src="img/bizagi-logo.png" width="180px" height="80px"> 
	<h2>Solicitud para Trámite de Factibilidad de Servicios</h2>
	<table id="psup" name="psup" border=0 class="psup">
	<tr>
	<td>
	<table id="info" name="info" border=0 class="info">
		<tr><td><strong>Dirección Divisional de Factibilidades</strong></td></tr>
		<tr><td><strong>Avenida 5 de Febrero No. 35</strong></td></tr>
		<tr><td><strong>Col. Las Campanas, C.P. 76010</strong></td></tr>
		<tr><td><strong>Santiago de Querétaro, Qro.</strong></td></tr>
		<tr><td><strong>Tel. 211-0600 Ext.1216</strong></td></tr>
	</table>
		</td>
		<td>
		<table id="fraccionamiento" name="fraccionamiento" border=1 class="fraccionamiento">
		<tr><td colspan=2 class="titulofraccionamiento">Fraccionamiento</td></tr>
		<tr><td>No. Expediente</td><td></td></tr>
		<tr><td>Fecha</td><td><?php echo $fecha;?></td></tr>
		<tr><td>Trámite</td><td>Solicitud de Factibilidad Nueva</td></tr>
		</table>
		</td>
	</tr>
	</table>

	<table class="DatosGenerales" name="DatosGenerales" id="DatosGenerales" border=1>
		<tr><td colspan=6 class="tituloDatosGenerales">Datos Generales</td></tr>
		<tr><td colspan=2>Nombre o razón social: </td><td colspan=4><?php echo $row['Nombre']; ?></td></tr>
		<tr><td colspan=4>Registro federal de causantes-Persona moral: ([<?php  $si_persona=$row['Personamoral']; if ($si_persona==1){echo "Si";}else{echo "No";}?>])</td><td colspan=2><?php echo $row['RFC']; ?></td></tr>
		<tr><td>Domicilio:</td><td colspan=5></td></tr>
		<tr><td>Colonia:</td><td colspan=3><?php echo $row['Coloniafiscal'];?></td><td>Código postal</td><td><?php echo $row['CPFiscal']; ?></td></tr>
		<tr><td>Municipio:</td><td><?php $muni=$row['MunicipioFiscal']; $consM = "SELECT * FROM Municipios where idMunicipios = $muni";$registroM = sqlsrv_query($conn,$consM);$rowM = sqlsrv_fetch_array( $registroM, SQLSRV_FETCH_ASSOC); echo $rowM['Nombre']; ?></td><td>Estado</td><td><?php echo $row['LocalidadFiscal']; ?></td><td>Teléfono(s)</td><td><?php echo $row['Telefono']; ?></td></tr>
		
		<tr><td colspan=2>Nombre del Representante Legal:</td><td colspan=4><?php echo $nomRep; ?></td></tr>
		<tr><td>Domicilio:</td><td colspan=5></td></tr>
		<tr><td>Colonia:</td><td colspan=3></td><td>Código postal</td><td></td></tr>
		<tr><td>Municipio:</td><td></td><td>Estado</td><td></td><td>Teléfono(s)</td><td><?php echo $telRep; ?></td></tr>
	</table>
	<table class="DatosPredio" name="DatosPredio" id="DatosPredio" border=1>
		<tr><td colspan=4 class="tituloDatosPredio">Datos del predio</td></tr>
		<tr><td width="400px">Nombre del faccionamiento:</td><td colspan=3><?php echo $nomfracc;?></td></tr>
		<tr><td>Superficie total (m2):</td><td><?php echo $superficie; ?></td><td width="160px">Clave catastral:</td><td><?php  echo $claveCat;?></td></tr>
		<tr><td colspan="2">Cambia nombre fraccionamiento:</td><td colspan=2><?php $constru=$regCon;  $consC = "SELECT * from TipoRegimen where idTipoRegimen = $constru"; $registroC=sqlsrv_query($conn,$consC);$rowC = sqlsrv_fetch_array( $registroC, SQLSRV_FETCH_ASSOC); echo $rowC['Regimenconstruccion'];?></td></tr>
		<tr><td>Tipo de vivienda:</td><td colspan=3><?php $vivi=$tipoV; $consV = "SELECT * FROM Tipovivienda WHERE idTipovivienda = $vivi"; $registroV=sqlsrv_query($conn,$consV);$rowV = sqlsrv_fetch_array( $registroV, SQLSRV_FETCH_ASSOC); echo $rowV['Tipo'];?></td></tr>
		<tr><td>Domicilio:</td><td colspan=3><?php echo  $domP; ?></td></tr>
		<tr><td>Colonia:</td><td colspan=3><?php echo $colP; ?></td></tr>
		<tr><td>Municipio:</td><td><?php $muni=$munP; $consMu = "SELECT * FROM Municipios WHERE idMunicipios = $muni"; $registroMu=sqlsrv_query($conn,$consMu);$rowMu = sqlsrv_fetch_array( $registroMu, SQLSRV_FETCH_ASSOC); echo $rowMu['Nombre']; ?></td><td>Estado</td><td>Queretaro</td></tr>
	</table>
	
	<table id="pinf" name="pinf" class="pin">
	<tr>
	<td class="colPin1">
		<table class="DatosDocumento" name="DatosDocumento" id="DatosDocumento">
			<tr class="trReq"><td class="tituloDatosPredio">Documento</td><td class="colent"></td></tr>
			<tr><td class="colDoc"><strong>Tipo Documento</strong></td><td class=""><strong>Aplica</strong></tr>
			<tr><td>Solicitud Para Tramite de Factibilidad de servicios</td><td>Si</tr>	
			<tr><td class="colDoc"><?php echo $doc1; ?></td><td class="colent">Si</tr>
			<tr><td><?php echo $doc2; ?></td><td>Si</tr>
			<tr><td><?php echo $doc3; ?></td><td>Si</tr>
			<tr><td><?php echo $doc4; ?></td><td>Si</tr>
			<tr><td><?php echo $doc5; ?></td><td>Si</tr>
			<tr><td>Copia certificada del acta constitutiva con datos del Registro Publico Federal (Personas Morales)</td><td>Si</tr>	
			<tr><td>Copia certificada poder notarial del representante legal</td><td>Si</tr>
			<tr><td>Copia de Inscripcion en el R.F.C</td><td>Si</tr>
			<tr><td>Copia Identificación Oficial del propietario</td><td>Si</tr>
			<tr><td>Copia Identificacion Oficial del Representante Legal</td><td>Si</tr>
			
		</table>
	</td>
	<td >
	<table class="DatosReqAgua" name="DatosReqAgua" id="DatosReqAgua">
		<tr class="trReq"><td class="titulore">Requerimiento de agua</td><td class="trReq"></td></tr>
		<tr><td>Doméstico</td><td><?php echo $dom; ?></td></tr>
		<tr><td>Comercial</td><td><?php echo $com; ?></td></tr>
		<tr><td>Industrial</td><td><?php echo $ind; ?></td></tr>
		<tr><td>Otro </td><td><?php echo $otr; ?></td></tr>
		<tr><td>Total de unidades a servir</td><td><?php echo $totuni; ?></td></tr>
	</table> 
	<p class="folioc"><?php echo $folio;?></p>
	</td>
	</tr>
	</table>

<div>
	<div class="divuno">
		<p>Atentamente:</p><br>
		<p>_______________________</p>
		<p>Firma del solicitante</p>
	</div>
	
	<div class="divdos">
		<p>Recibí:</p><br>	
		<p>________________________</p>	
	</div>
	
	
	
</div>
	
</body>
</html>