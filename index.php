<?php 
error_reporting(0);
include_once 'conexion.php';
include 'obtenerMunicipios.php';

$rfc=$_GET['rfc'];

$consulta = "SELECT [idSolicitudPadron],[Folio],WC.idCase,[Fecha],[Tipomovimiento],[Usuario],WI.idTask
  ,T.tskDisplayName
  ,S.Correcto
  FROM [SCG].[dbo].[SolicitudPadron] S
  inner join [SCG].[dbo].[WFCASE] WC
  on S.Folio = WC.radNumber
  inner join [SCG].[dbo].[WORKITEM] WI
  on wC.idCase=WI.idCase
  inner join SCG.dbo.TASK T
  on WI.idTask=T.idTask
  inner join SCG.dbo.WFUSER WF
  on S.Usuario = WF.idUser
  where WF.userName = '$rfc'
  and WI.idTask=5096
  and S.Correcto=0
  and S.Requiererevision=1
  group by [idSolicitudPadron],[Folio],WC.idCase,[Fecha],[Tipomovimiento],[Usuario],WI.idTask
  ,T.tskDisplayName
  ,S.Correcto ";


  $registro = sqlsrv_query($conn,$consulta);

  $results = array();
    while( $row = sqlsrv_fetch_array( $registro, SQLSRV_FETCH_ASSOC) ) 
    {
         $results[] = $row;
    }

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Nueva Factibilidad</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    
  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    
    <link rel="stylesheet" href="css/estilo.css"/>

    <script src="js/jquery.js"></script><!--- version 3.5.1 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="js/ajax.js"></script>
   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.8.0/sweetalert2.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script defer src="https://friconix.com/cdn/friconix.js"> </script>
    <script type="text/javascript" src="js/ajaxEditar.js"></script>
      <script>

      $(document).ready(function(){
         $(document).on('click','.add',function(){
       
        var html ='';
        html += '<tr>';
        html += '<td><input type="text" name="rfcCo[]" class="form-control inputsCo" placeholder="RFC del copropietario."/></td>';
        html += '<td><input type="text" name="nombreCo[]" class="form-control inputsCo" placeholder="Coloque el nombre del copropietario"/></td>';
        html += '<td><input type="text" name="calleCo[]" class="form-control inputsCo" placeholder="Calle del copropietario"/></td>'; 
        html += '<td><input type="text" name="noextCo[]" class="form-control inputsCo" placeholder="Numero exterior del copropietario"/></td>'; 
        html += '<td><input type="text" name="nointCo[]" class="form-control inputsCo" placeholder="Numero interior del copropietario"/></td>'; 
        html += '<td><input type="text" name="coloniaCo[]" class="form-control inputsCo" placeholder="Colonia del copropietario"/></td>';
        html += '<td><select name="municipioCo[]" class="form-control inputsCo"><option value="0">- - -</option><?php echo obtenerDatos();?></select></td>';
        html += '<td><button type="button" name="remove" class="remove btn btn-danger">-</button></td>';

        $('#bodyCo').append(html);


    });

         $(document).on('click','.remove',function(event){
           event.preventDefault();
          Swal.fire({
            title: 'Estas Seguro?',
            text: "no podras revertir esta accion!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar!'
            }).then((result) => {
                if (result.isConfirmed) {
                  $(this).closest('tr').remove();
                  Swal.fire(
                    'Eliminado!',
                    'La fila fue eliminada.',
                    'success'
    )
  }
})
          
           
         });
  });//close document
         
    </script>
      
    </head>
  <body>
    
    
<!-- <nav class="navbar fixed-top bg-dark flex-md-nowrap p-0 barra minav">
  <a class="col-sm-3 col-md-2 mr-0"><img src="img/bizagi-logo.png" alt="" height=45px; width=155px;></a>
  <ul class="navbar-nav px-3">
    <li class="nav-item text-nowrap">
    </li>
  </ul>
</nav> -->

<div class="container">
  <div class="row principal">
    <nav class="col-md-2">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a id="btnNuevo" class="btn btn-primary">
              <span data-feather="home"></span>
              <strong><i class="fas fa-plus"> Nueva solicitud</i></strong><span></span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
      
      
  <main role="main"class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="container">
      <div class="table-wrapper">
        <div class="table-title">
          <h1><strong>Bandeja de Entrada</strong></h1>
        </div>
        <div class='clearfix'></div>
        <div id="loader"></div><!-- Carga de datos ajax aqui -->
        <div id="resultados">
          <input type="hidden" id="folio" name="folio" value="<?php echo $_GET['rfc']; ?>" placeholder="">  
          <hr>
          <table id="tblCasos" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Numero Caso</th>
                <th>Folio</th>
                <th>Fecha</th>
                <th>Proceso</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody>
              <?php  foreach($results as $dat) { ?> 
              <tr>
                <td><?php echo $dat['idCase']; ?></td>
                <td><?php echo $dat['Folio']; ?></td>
                <td><?php $date=$dat['Fecha']; echo date_format($date, 'Y-m-d'); ?></td>
                <td><?php echo $dat['tskDisplayName']; ?></td>
                <td><button type="button" class="editar btn btn-primary" id="<?php echo $dat['Folio']; ?>"><i class="fa fa-pencil"></i></button></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>  
         </div>      
      </div>
    </div>    
  </main>

<!--//////////////////////////////////////////////////////modal agregar/////////////////////////////////////////////////////////// -->
<?php require_once 'control.php'; ?>
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="titulo"><strong></strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h4>Coloca el correo </h4>
        <form id='alta-caso' name='miformulario' class="form-register" method='POST' enctype='multipart/form-data'>
        <div class='row'>
            <div class='col-md-6'>
                <input type="hidden" id="rfc" name="rfc" value="<?php $rfc = $_GET['rfc']; echo $rfc; ?>">
                <input type='hidden' id='intramite' name='tramite' value='101' class='form-control' readonly='readonly' />
                <input type='hidden' id='movimiento' name='movimiento' value='7' class='form-control' readonly='readonly' />
                <label for="radNumber">Folio:</label>
                <input type="text" id="radNumber" name="radNumber" class="form-control" value="" readonly="readonly" />
            </div>
            <div class="col-md-6">
              <label>Fecha:</label>
              <div><?php echo fechaSer()?></div>
              <div><?php echo obteneridRFC()?></div>
            </div>
        </div>
        <div class="row">
          <div class='col-md-12'>
                <label>Correo electronico para notificar:</label><span class="obligado">*</span>
                <input type='email' id='inEmail' name='correo' class='form-control' placeholder='Ejemplo@sudominio.com' required>             
            </div>
        </div>    
        <hr>    
        <div class="row">
          <div class="col-md-8">
            <h4><strong>Datos del predio</strong></h4> 
          </div>
          <div class="form-group col-md-4">
            <h4><strong>Requerimiento Agua</strong></h4> 
          </div>
        </div>
        <div class="row">
          <div class='col-md-4'>
            <label>Nombre del Fraccionamiento</label><span class="obligado">*</span>
            <input type='text' id='nomfracc' name='nomFracc' class='form-control' placeholder="Escribe el nombre del fraccionamiento" />
            <label>Clave Catastral:</label><span class="obligado">*</span>
            <input type="text" id="clvCat" name="clvCat" class='form-control' placeholder="Coloca clave catastral">
            <label>Tienes otras claves catastrales?</label>
            <br>
            <div class="radio">
              <input type="radio"  name="check" id="check" value="1" onchange="showContent()">
              <label for="check">Si</label>

              <input type="radio" name="check" id="check1" value="0" onchange="ocultarContent()" checked>
              <label for="check1">No</label>
            </div>
            
            
            <div id="content" style="display: none;">
              <table class="table tablados">
                <thead>
                  <tr>
                    <td>Clave Catastral</td>
                  </tr>
                </thead>
                <tbody>
                  <tr id="filas-base">
                    <td><input type="text" name="txtclvCat[]" class="form-control" placeholder="Escribe tu clave" /></td>   
                  </tr>
                </tbody>
              </table> 
              <input id="agregartablados" class="btn btn-primary" type="button" value="+" onclick="AgregarCampo()" /> 
            </div>
            <br>
            <label>Tipo Vivienda</label><span class="obligado">*</span>
            <select id="tipviv" class="form-control" name="tipviv">
              <option value="0">Selecciona Tipo Vivienda...</option>
                <?php 
                  $query="SELECT idTipovivienda, Tipo FROM dbo.Tipovivienda where Tipo is not null ORDER BY Tipo";
                  $resultado=sqlsrv_query($conn,$query);  
                  while($fila = sqlsrv_fetch_array($resultado))
                  {
                ?>
                    <option value=" <?php echo $fila['idTipovivienda']?>"><?php echo $fila['Tipo']?></option>
                <?php 
                  }
                ?>
            </select>
            <label>C.P</label>
            <input type="text" id="cp" name="cp" class='form-control' placeholder="Ejemplo 76000"/>
            <label>Telefono:</label>
            <input type="tel" id="tele" name="tele" class='form-control' placeholder="Coloca tu telefono">
            <label>Tiene Copropietarios?</label>
            <br>
            <div class="radio">
              <input type="radio" name="checkCo" id="checkCo" value="1" onchange="mostrarCop()">
              <label for="checkCo">Si</label>

              <input type="radio" name="checkCo" id="checkCo1" value="0" onchange="ocultarCo()" checked>
              <label for="checkCo1">No</label>
            </div>
            
            
            <br>
          </div>
          <div class='col-md-4'>
              <label>Superficie</label> <span class="obligado">*</span>
              <input type="text" id="sup" name="sup" class='form-control' placeholder="Teclee la superficie">
              <label>Regimen Construcción</label><span class="obligado">*</span> 
              <select id="regcons" name="regcons" class="form-control">
              <option value="0">Selecciona Tu Regimen...</option>
                <?php
                  $queryR="SELECT idTipoRegimen, Regimenconstruccion FROM dbo.TipoRegimen where Regimenconstruccion is not null ORDER BY Regimenconstruccion";
                  $resultadoR=sqlsrv_query($conn,$queryR);
                  while($fila1 = sqlsrv_fetch_array($resultadoR))
                    {
                ?>
                      <option value=" <?php echo $fila1['idTipoRegimen']?>"><?php echo $fila1['Regimenconstruccion']?></option>
                <?php 
                    }
                ?>
              </select>
              <label>Domicilio:</label><span class="obligado">*</span>
              <input type="text" id="domi" name="domi" class='form-control' placeholder="Coloque su domicilio" />
              <label>Colonia:</label><span class="obligado">*</span>
              <input type="text" id="col" name="col" class='form-control' placeholder="Introduzca su colonia" />
              <label>Municipio:</label><span class="obligado">*</span> 
              <select id="mun" name="mun" class="form-control">
              <option value="0">Seleccione Municipio...</option>
                <?php
                  $queryMun="SELECT idMunicipios,Nombre from Municipios where IdEstado=22 and Nombre is not null";
                  $resultadoMu=sqlsrv_query($conn,$queryMun);
                  while($filaMU = sqlsrv_fetch_array($resultadoMu))
                  {
                ?>
                    <option value=" <?php echo $filaMU['idMunicipios']?>"><?php echo $filaMU['Nombre']?></option>
                <?php 
                  }
                ?>
              </select>
              <label>Correo:</label>
              <input type="email" class="form-control" id="correopr" name="correopr" placeholder="Introduzca su correo">        
          </div>
          <div class='col-md-4'>
            <div class="row">
              <div class="col-md-6">
                <h3>Uso</h3>
                <br/>
                <label>Domestico</label><span class="obligado">*</span>
                <br/>
                <br/>
                <label>Comercial</label><span class="obligado">*</span>
                <br/>
                <br/>
                <label>Industrial</label><span class="obligado">*</span>
                <br/>
                <br/>
                <label for="otros">Otros</label>
                <input type="number" id="otross" class="form-control form-control-sm" name="otross" >
                <br>
                 <label>Total de unidades</label>
              </div>
              <div class="col-md-6">
                <h3>Unidades</h3>
                <br/>
                <input type="number" class="form-control form-control-sm amt" id="domes" name="domes" value="0" />
                <br/>
                <input type="number" class="form-control form-control-sm amt" id="comer" name="comer" value="0" />
                <br/>
                <input type="number" class="form-control form-control-sm amt" id="ind" name="ind" value="0" />   
                <br>
                <br>
                <input type="number" class="form-control form-control-sm amt" id="otros" name="otros" value="0"> 
                <br>
                <br>
              <input type="number" id="totuni" name="totuni" class="form-control" value="" readonly />
              </div>
              
            </div>      
         </div>

          <div class='col-md-12'>
          <br>
          <div id="contentCo" style="display: none;">
            <div class="table-responsive">
              <table class="table table-hover tablaCo">
              <thead>
                <tr>
                  <td>RFC</td>
                  <td>Nombre o Razon social</td>
                  <td>Calle</td>
                  <td>No. ext</td>
                  <td>No. int</td>
                  <td>Colonia</td>
                  <td>Municipio</td>
                  <td></td>
                </tr>
              </thead>
              <tbody id="bodyCo"></tbody>
            </table> 
            <input id="agregartablaCo" class="add btn btn-primary" type="button" value="+" /> 
            </div>
            
          </div>
         </div>
      
    <!--representante legal-->
        <div class='form-group col-md-12'>
        <h2>Representante Legal</h2>
        <h4>Escriba los siguientes datos del representante legal</h4>
        <label>¿Se promueve y representa en propio derecho?</label><span class="obligado">*</span>
        <br>
        <div class="radio">
          <input type="radio" id="pregRep" name="pregRep" value="1">
          <label for="pregRep">Si</label>

          <input type="radio" id="pregRepN" name="pregRep" value="0">
          <label for="pregRepN">No</label>
        </div>
         
        
         <br>
         <label>Tipo de Representante</label><span class="obligado">*</span>
          <select id="tipRep" class="form-control rep" name="tipRep">
           <option value="0">Seleccione el Tipo de Representante</option>
          <?php 
            $queryRep="SELECT idTiporepresentante,DescripciondelTipo from Tiporepresentante where DescripciondelTipo is not null";
            $resultadoRep=sqlsrv_query($conn,$queryRep);  
            while($filaRep = sqlsrv_fetch_array($resultadoRep))
           {
            ?>
             <option value=" <?php echo $filaRep['idTiporepresentante']?>"><?php echo $filaRep['DescripciondelTipo']?></option>
            <?php 
            }
           ?>
         </select>
         <label>No. Documento</label><span class="obligado">*</span>
         <input type="text" id="numDoc" name="numDoc" class="form-control rep" placeholder="Coloque su numero de Documento">
         <label>Tipo de Documento(Acredite personalidad)</label><span>*</span>
          <select id="tipdoc" name="tipdoc" class="form-control rep">
            <option value="0">Seleccione Tipo de Documento...</option>
             <?php
            $queryT="SELECT * FROM Tipodocumentos WHERE Categoria=53 and Tipotramite=1 and Tipomovimiento=7 ORDER BY Documento";
            $resultadoT=sqlsrv_query($conn,$queryT);    
            while($filaT = sqlsrv_fetch_array($resultadoT))
           {
            ?>
             <option value=" <?php echo $filaT['idTipodocumentos']?>"><?php echo $filaT['Documento']?></option>
            <?php 
            }
           ?>
          </select>
          <br>
    <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col"><label>Nombre:</label><span class="obligado">*</span></th>
      <th scope="col"><label>Telefono:</label><span class="obligado">*</span></th>
      <th scope="col"><label>Correo:</label><span class="obligado">*</span></th>
      <th hidden="hidden" scope="col"><label>Seleccionado:</label></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"><input type='text' id='nombre' name='nombre' class='form-control' /></th>
      <td><input type='text' id='telefono' name='telefono' class='form-control' maxlength="10" /></td>
      <td><input type='email' id='correoREP' name='correoREP' class='form-control entrada-usuario' /></td>
      <td hidden="hidden"><label><input type="radio" id="sir" name="seleccionado" value="1" checked="checked"> Si</label>
      </td>
    </tr>
   </tbody>
 </table>
</div>

<br>

<!--  <input type='button' id='mostrar tabla' name='Imprime solicitud' value='Relacion Documentos' class='btn btn-primary'  onclick='mostrarTabla()'/> -->
<div class="col-md-12" id="tablaDoc">
  <!--Documentos-->
   <h4>Documentos del Predio</h4>
   <br><!--aqui va la tabla de documentos.-->   
  <div id="tablaDoc1">
  <p class="aviso">Favor de colocar solo archivos con extensión .pdf y no mayores a 5 MB.</p>
  <table id="tablaDoc11" class="table table-hover" >
  <thead>
    <tr>
      <th scope="col">Documento</th>
      <th scope="col">Anexo</th>
    </tr>
  </thead>
  <tbody>
    <tr>
     <th><label>Solicitud para trámite de factibilidad de servicios</label><span class="obligado">*</span></th>
      <td><input type="file" id="filePickerD" name="archivoD" class="form-control inputfile inputfile-7" accept=".pdf" >
         <label for="filePickerD"> <span id="ar1">Cargar Archivo</span></label>
        <br>
        <input type="hidden" id='base64textareaD' name='code1D' class='form-control' /></td>
    </tr>
    <!--1er documento-->
    <tr>
     <th><label>Informe Uso Suelo</label><span class="obligado">*</span></th>
      <td><input type="file" id="filePickerD1" name="archivoD1" class="form-control inputfile inputfile-7" accept=".pdf" />
        <label for="filePickerD1"> <span id="ar2">Cargar Archivo</span></label>
        <br>
        <input type="hidden" id='base64textareaD1' name='code1D1' class='form-control' /></td>
    </tr>
    <!--2do documento-->
    <tr>
     <th><label>Archivo polígono KML</label><span class="obligado">*</span></th>
      <td><input type="file" id="filePickerD2" name="archivoD2" class="form-control inputfile inputfile-7" accept=".kml,.kmz" >
        <label for="filePickerD2"> <span id="ar3">Cargar Archivo</span></label>
        <br>
        <input type='hidden' id='base64textareaD2' name='code1D2' class='form-control' /></td>
    </tr>
  <!--3er documento-->
    <tr>
     <th><label>Croquis predio plan parcial desarrollo</label><span class="obligado">*</span></th>
      <td><input type="file" id="filePickerD3" name="archivoD3" class="form-control inputfile inputfile-7" accept=".pdf" >
        <label for="filePickerD3"> <span id="ar4">Cargar Archivo</span></label>
        <br>
        <input type='hidden' id='base64textareaD3' name='code1D3' class='form-control' /></td>
    </tr>
      
      <!--4to documento-->
      <tr>
    <th><label>Copia certificada de la escritura de propiedad del predio</label><span class="obligado">*</span></th>
        <td>
          <input type="file" id="filePickerD4" name="archivoD4" class="form-control inputfile inputfile-7" accept=".pdf" >
          <label for="filePickerD4"> <span id="ar5">Cargar Archivo</span></label>
          <br>
          <input type='hidden' id='base64textareaD4' name='code1D4' class='form-control'/></td>
    </tr>

       <!--5to documento-->
      <tr>
     <th><label>Croquis referencia de calles (Google mapa color)</label><span class="obligado">*</span></th>
        <td>
          <input type="file" id="filePickerD5" name="archivoD5" class="form-control inputfile inputfile-7" accept=".pdf" >
          <label for="filePickerD5"> <span id="ar6">Cargar Archivo</span></label>
          <br/>
          <input type='hidden' id='base64textareaD5' name='code1D5' class='form-control'/>
          </td>
      </tr>
          <!-- 6to documento -->
 <!--  <tr id="sexto" style="display: none;">
     <td scope="row"><input type="text" id="doc5" name="doc5" value="copia certificada contrato de fideicomiso,en caso de modificacion anexar copia certificada de convenios modicarios" class="form-control sinborde doc" disabled="disabled" ><span>*</span></td>
        <td>
          <input type="file" id="filePickerD4" name="archivoD4" class="form-control" accept=".pdf" onchange="ficheroD4.value=this.value.replace('C:\\fakepath\\', '');">
          <input type="hidden" id="ficheroD4" name="ficheroD4" class="form-control" />
          <br/>
          <input type='hidden' id='base64textareaD4' name='code1D4' class='form-control'/>
          </td>
      </tr> -->

  </tbody>
</table>

      </div>          
      </div>

      <h3>Documentos del desarrollador</h3>
      <?php
  $rfc=$_GET['rfc'];
  

$docDes='';

echo '<table id="tblCasos" class="table table-striped table-bordered" cellspacing="0" width="100%">';
    echo '<thead>';
    echo '<tr>';
            echo'<th>Documento</th>';
            echo'<th>Anexos</th>';
            echo'<th>Es correcto</th>';
        echo '</tr>';
        echo '</thead>';

  $str_queryPD="SELECT idPadronDesarrolladores
  FROM [SCG].[dbo].PadronDesarrolladores
  where RFC='".$rfc."'";
  //echo $str_queryPD;
  $queryPD = sqlsrv_query($conn, $str_queryPD);   
  $Padron = sqlsrv_fetch_array($queryPD);
  $idPadronDesarrolladores=$Padron['idPadronDesarrolladores'];
  
  //echo "ID PADRON: ".$idPadronDesarrolladores;
  
  $str_queryD="SELECT idDocumentos
  FROM [SCG].[dbo].[Documentos]
  where (Tipo=8 or Tipo=10 or Tipo=11) AND 
  PadronDesarrolladores=$idPadronDesarrolladores";
  
  $queryD = sqlsrv_query($conn, $str_queryD);   
  while($Docto=sqlsrv_fetch_array($queryD)){
  $idDocto=$Docto['idDocumentos'];
  
  //echo "<br>ID DOCTOS: ".$idDocto;
  
  $consulta_doctos="SELECT idFileUpload,fuIdParent,idAttrib,fuFileName
  FROM [SCG].[dbo].[BAFILEUPLOAD] where fuIdParent=$idDocto and idAttrib=10031";
  $query_doctos = sqlsrv_query($conn, $consulta_doctos) or die("<h3><span style='color: #003366;'><strong>No se encontraron resultados.</strong></span></h3>");


  while($Cons_doctos=sqlsrv_fetch_array($query_doctos)){  
  //var_dump($Cons_doctos);
  $nombrearc=utf8_encode($Cons_doctos['fuFileName']);
  $idDocto2=$Cons_doctos['fuIdParent'];
  
  $wholepart=($Cons_doctos['fuIdParent']-($Cons_doctos['fuIdParent'] % 1000))/1000;
  //echo "WHOLEPART ".$wholepart."<br>";
  $rutaArc="//10.1.1.155/Docs/10006/".$wholepart."/".$idDocto2."/10031/";
  
  
  //$results[]=$Cons_doctos;
  //"<a target='_blank' href='abrirDocto.php?nombreDeArchivo='.utf8_decode($rutaArc.$nombrearc).''>'.utf8_decode($nombrearc).'</a>';
  if ($Cons_doctos['fuIdParent'] == 702) {
    $docDes ='Copia certificada poder notarial del representante legal';
  }elseif ($Cons_doctos['fuIdParent'] == 703) {
    $docDes ='Identificación Oficial del propietario';
  }elseif ($Cons_doctos['fuIdParent'] == 704) {
    $docDes ='Oficial del Representante Legal';
  }else{
    $docDes='';
  }

        echo '<tbody>';   
          echo '<tr>';
            echo '<td>'.$docDes.'</td>';
            echo '<td><a target="_blank" href="abrirDocto.php?nombreDeArchivo='.utf8_decode($rutaArc.$nombrearc).'">'.utf8_decode($nombrearc).'</a></td>';
            echo '<td><input type="checkbox" name="entregado" value=""></td>';
          echo '</tr>';
        echo '</tbody>';  

          
  }
  }
echo '</table>' 
?>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" id="vacio" name="vacio">  
       <div id="precarga"></div>
       <input type='button' id='btn_enviar' name='creaCaso' value='Enviar Solicitud' class='btn btn-primary' onclick='realizaProceso()'/>
      
      <!--<input type='button' id='guardar' name='guarda_btn' value='GrabarDatos' class='btn btn-secondary' onclick='realizaProcesoGuardar()'/>-->
      <input type='hidden' id='transaccion'name='transaccion' value='insertar'>
      </div>
      </div>
    </div>
  </div>
 </div>   
</div>
</form>
<!--////////////////////////////////////////////////// cierra modal agregar///////////////////////////////////////-->

<script>
          var handleFileSelect = function(evt) {
            var files = evt.target.files;
            var file = files[0];
        
            if (files && file) {
                var reader = new FileReader();
        
                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    document.getElementById("base64textareaD").value = btoa(binaryString);
                };
        
                reader.readAsBinaryString(file);
            }
        };
        
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('filePickerD').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
</script>   

<script>
          var handleFileSelect = function(evt) {
            var files = evt.target.files;
            var file = files[0];
        
            if (files && file) {
                var reader = new FileReader();
        
                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    document.getElementById("base64textareaD1").value = btoa(binaryString);
                };
        
                reader.readAsBinaryString(file);
            }
        };
        
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('filePickerD1').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
</script>   

<script>
          var handleFileSelect = function(evt) {
            var files = evt.target.files;
            var file = files[0];
        
            if (files && file) {
                var reader = new FileReader();
        
                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    document.getElementById("base64textareaD2").value = btoa(binaryString);
                };
        
                reader.readAsBinaryString(file);
            }
        };
        
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('filePickerD2').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
</script>   

<script>
          var handleFileSelect = function(evt) {
            var files = evt.target.files;
            var file = files[0];
        
            if (files && file) {
                var reader = new FileReader();
        
                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    document.getElementById("base64textareaD3").value = btoa(binaryString);
                };
        
                reader.readAsBinaryString(file);
            }
        };
        
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('filePickerD3').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
</script>  

<script>
          var handleFileSelect = function(evt) {
            var files = evt.target.files;
            var file = files[0];
        
            if (files && file) {
                var reader = new FileReader();
        
                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    document.getElementById("base64textareaD4").value = btoa(binaryString);
                };
        
                reader.readAsBinaryString(file);
            }
        };
        
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('filePickerD4').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
</script>

<script>
          var handleFileSelect = function(evt) {
            var files = evt.target.files;
            var file = files[0];
        
            if (files && file) {
                var reader = new FileReader();
        
                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    document.getElementById("base64textareaD5").value = btoa(binaryString);
                };
        
                reader.readAsBinaryString(file);
            }
        };
        
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('filePickerD5').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
</script>    



<!--//////////////////////////////////////////////////modal editar///////////////////////////////////////////////////-->
<?php
require_once("editar.php");
?>  
  
<!--////////////////////////////////////////////////modal editar//////////////////////////////////////////////////////-->

  <script>
     $('.popover-dismiss').popover({
  trigger: 'focus'
})
      </script>
      




<!--////////////////////////////////////////////// scripts para modal editar///////////////////////////////////7-->

  
</body>
</html>        