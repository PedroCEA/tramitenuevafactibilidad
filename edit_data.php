<?php 

require_once 'conexion.php';

if(isset($_POST['edit_id'])){
	$folio=$_POST['edit_id'];

$obCase="select * from wfcase where radNumber='$folio'";	

 $sql1=sqlsrv_query($conn,$obCase);
  $result = sqlsrv_fetch_array($sql1);
 

$caso=$result['idCase'];


$consulta="select top 1 ex.idExpediente,sp.Folio,sp.Usuario,sp.Tipotramite,Tipomovimiento,sp.Correo,sp.Fecha,sp.Serepresenta,
sp.TipoRepresentante,ex.Cvescatastrales,ex.Desarrollo,ex.Cvecatastral,ex.Existefideicomiso,ex.Copropiedad,ex.Superficie,ex.Tipovivienda,ex.Regimenconstruccion,ex.Domicilio,ex.Colonia,
ex.Municipio,ex.CP,ex.Telefono,ex.Correo as correopr,ex.Reqdomestico,ex.Reqcomercial,ex.Reqindustrial,ex.Reqmixto,ex.Totalunidades,
c.Nodocumento,c.Tipodocumento,r.Nombre,r.Telefono as telefonorep,r.Correo as correorep,r.Seleccionado
from Expediente ex 
inner join SolicitudPadron sp on ex.SolicitudPadron=sp.idSolicitudPadron
inner join Cedula c on ex.Cedula=c.idCedula
inner join Representantes r on ex.idExpediente=r.Expediente
where sp.Folio='$folio'";

$sql=sqlsrv_query($conn,$consulta);

	while ($row=sqlsrv_fetch_array($sql)) {
		$expediente=$row['idExpediente'];
		$folioed=$row['Folio'];
		$fechaed=$row['Fecha']->format('Y-m-d');
		$correoed=$row['Correo'];
		$fracc=$row['Desarrollo'];
		$clvCated=$row['Cvecatastral'];
		$otrasClvs=$row['Cvescatastrales'];
		$tipoV=$row['Tipovivienda'];
		$cped=$row['CP'];
		$teled=$row['Telefono'];
		$correopred=$row['correopr'];
		$tieneCO=$row['Copropiedad'];
		$suped=$row['Superficie'];
		$rConsed=$row['Regimenconstruccion'];
		$domied=$row['Domicilio'];
		$coled=$row['Colonia'];
		$muned=$row['Municipio'];
		$existeFideo=$row['Existefideicomiso'];
		$rDomesed=$row['Reqdomestico'];
		$rComered=$row['Reqcomercial'];
		$rIndused=$row['Reqindustrial'];
		$rmixed=$row['Reqmixto'];
		$rToted=$row['Totalunidades'];
		$srep=$row['Serepresenta'];
		$tipRep=$row['TipoRepresentante'];
		$nDoc=$row['Nodocumento'];
		$tDoc=$row['Tipodocumento'];
		$nombre=$row['Nombre'];
		$telRep=$row['telefonorep'];
		$correorep=$row['correorep'];
		$sele=$row['Seleccionado'];
		$usuario=$row['Usuario'];
	}


$consulta="SELECT userName FROM WFUSER WHERE idUser LIKE '$usuario'";

  $sql=sqlsrv_query($conn,$consulta);
  $values = sqlsrv_fetch_array($sql);

$userrfc=$values['userName'];
	
}

$consultaObservaciones="SELECT TOP 1 Observacion FROM Observaciones WHERE Usuario = $usuario order by idObservaciones desc";

$sqlOb=sqlsrv_query($conn,$consultaObservaciones);
$valuesOb = sqlsrv_fetch_array($sqlOb);
$obser=$valuesOb['Observacion'];

?>
<h4>Coloca el correo </h4>
        <form id='edita-caso' name='miformulario' method='POST' enctype='multipart/form-data'>
      <div class='form-group row'>
            <div class='form-group col-md-8'>
           <input type="hidden" name="caso" value="<?php echo $caso; ?>" >	
           <input type="hidden" id="rfced" name="rfced" value="<?php echo $userrfc; ?>">
           <input type='hidden' id='intramite' name='tramite' value='101' class='form-control' readonly='readonly' />
           <input type='hidden' id='movimiento' name='movimiento' value='7' class='form-control' readonly='readonly' />
           <div class="form-group col-md-6">
              <input type="text" id="radNumbered" name="radNumbered" class="form-control" value="<?php echo $folioed; ?>" readonly="readonly" />
           </div>
           
            </div>
          <label>Fecha:</label>
           <div><input type="date" id="fechaed" name="fechaed" class="form-control" value="<?php echo $fechaed; ?>" readonly/></div>
            <div><?php ?></div>
            <div class='form-group col-md-7'>
                <label>Correo electronico para notificar:</label><span>*</span>
                <input type='email' id='inEmailed' name='correoed' class='form-control correo' value="<?php echo $correoed; ?>" readonly >             
            </div>
             <div class='form-group col-md-5'>
                <label>Observaciones:</label>
                <input type='text' id='obser' name='obser' class='form-control correo' value="<?php echo $obser; ?>" readonly >             
            </div>
             <div class="form-group col-md-10">
              <div class="form-group row">
                <div class="form-group col-md-8">
                   <h4><strong>Datos del predio</strong></h4> 
                  </div>
                  <div class="form-group col-md-4">
                    <h4><strong>Requerimiento Agua</strong></h4> 
                  </div>
              </div>
             </div>
        <div class='form-group col-md-4'>
        <label>Nombre del Fraccionamiento</label><span>*</span>
         <input type='text' id='nomfracced' name='nomFracced' class='form-control' placeholder="Escribe el nombre del fraccionamiento" value="<?php echo $fracc; ?>" readonly/>
         <label>Clave Catastral:</label><span>*</span>
         <input type="text" id="clvCated" name="clvCated" class='form-control' placeholder="Coloca clave catastral" value="<?php echo $clvCated; ?>" readonly>
         <label>Tienes otras claves catastrales?</label>
        <br>
         <label>
         <input type="radio"  name="checked" id="checked" value="" onchange="showContented()" <?php if($otrasClvs==1) {echo "checked"; }else{echo "disabled";}?>>Si</label>
         <label>
         <input type="radio" name="checked" id="checked1" value="" onchange="ocultarContented()" <?php if($otrasClvs==0) {echo "checked"; }else{echo "disabled";}?>>No</label>
         <div id="contented">
           <table class="tablados">
            <thead>
              <tr>
                 <td>Clave Catastral</td>
              </tr>
             </thead>
             <tbody>
             	<?php $str_query="SELECT idClavescatastrales,Clave,Expediente
							FROM SCG.dbo.Clavescatastrales
							WHERE Expediente='".$expediente."'";
	
						$queryCve = sqlsrv_query($conn, $str_query);
						while($cvecatastral=sqlsrv_fetch_array($queryCve)){ 
						?>
               	<tr id="filas-base">
               	  		<td><input type="text" name="txtclvCat['Clave']" class="form-control" value="<?php echo $cvecatastral['Clave'];?>" readonly/></td>   
                </tr>
                <?php
						}
						?>
              </tbody>
        </table> 
        <input id="agregartablados" class="btn btn-primary" type="button" value="+" onclick="AgregarCampo()" readonly/> 
      </td>            
    </tr>
   </table>
         </div>
         <br>
         <label>Tipo Vivienda</label><span>*</span>
         <select id="tipviv" class="form-control" name="tipviv" readonly>
           <option value=""></option>
          <?php 
            $query="SELECT idTipovivienda, Tipo FROM dbo.Tipovivienda ORDER BY Tipo";
            $resultado=sqlsrv_query($conn,$query);  
            while($fila = sqlsrv_fetch_array($resultado))
           {

           	 $selected= ($tipoV == $fila['idTipovivienda'])?"selected":"";
            ?>

             <option value=" <?php echo $fila['idTipovivienda']?>" <?php echo $selected ?>><?php echo $fila['Tipo']?></option>
            <?php 
            }
           ?>
         </select>
         <label>C.P</label>
         <input type="text" id="cped" name="cped" class='form-control' placeholder="ejemplo 76000" value="<?php echo $cped;?>" readonly/>
         <label>Telefono:</label>
         <input type="tel" id="teleed" name="teleed" class='form-control' placeholder="" value="<?php echo $teled; ?>" readonly>
         <label>Tiene Copropietarios?</label>
         <br>
		
         <label>
         <input type="radio"  name="checkCo" id="checkCo" value="1" onchange="mostrarCop()" <?php if ($tieneCO==1) {echo "checked";}else{echo "disabled";}?>>Si</label>
         <label>
         <input type="radio" name="checkCo" id="checkCo1" value="0" onchange="ocultarCo()" <?php if ($tieneCO==0) {echo "checked";}else{echo "disabled";}?>>No</label>
        
         <br>
        </div>
        
        <div class='form-group col-md-3'>
         <label>Superficie</label> <span>*</span>
         <input type="number" id="suped" name="suped" class='form-control' value="<?php echo $suped; ?>" readonly>
         <label>Regimen Construcción</label><span>*</span> 
         <select id="regcons" name="regcons" class="form-control" readonly>
           <option value=""></option>
           <?php
            $queryR="SELECT idTipoRegimen, Regimenconstruccion FROM dbo.TipoRegimen ORDER BY Regimenconstruccion";
            $resultadoR=sqlsrv_query($conn,$queryR);
            
            while($fila1 = sqlsrv_fetch_array($resultadoR))
           {
           	 $selectedR= ($rConsed == $fila1['idTipoRegimen'])?"selected":"";
            ?>
             <option value=" <?php echo $fila1['idTipoRegimen']?>" <?php echo $selectedR ?>><?php echo $fila1['Regimenconstruccion']?></option>
            <?php 
            }
           ?>
         </select>
         <label>Domicilio:</label><span>*</span>
         <input type="text" id="domied" name="domied" class='form-control' placeholder="Coloque su domicilio" value="<?php echo $domied; ?>" readonly/>
         <label>Colonia:</label><span>*</span>
         <input type="text" id="col" name="col" class='form-control' value="<?php echo $coled; ?>" readonly/>
         <label>Municipio:</label><span>*</span> 
         <select id="mun" name="mun" class="form-control" readonly disabled>
           <option value=""></option>
           <?php
            $queryMun="SELECT idMunicipios,Nombre from Municipios where IdEstado=22";
            $resultadoMu=sqlsrv_query($conn,$queryMun);
            
            while($filaMU = sqlsrv_fetch_array($resultadoMu))
           {
           	 $selectedM= ($muned == $filaMU['idMunicipios'])?"selected":"";
            ?>
             <option value=" <?php echo $filaMU['idMunicipios']?>" <?php echo $selectedM ?>><?php echo $filaMU['Nombre']?></option>
            <?php 
            }
           ?>
         </select>
         <label>Correo:</label>
         <input type="email" class="form-control" id="correopred" name="correopred" placeholder="" value="<?php echo $correopred; ?>" readonly> 
         <label for="">Es Fideicomiso?</label>
         <br>
         <label>
         <input type="radio"  name="checkFi" id="checkFi" value="1" onchange="showContentDoc()" <?php if($existeFideo==1){echo "checked";}else{echo "disabled";} ?>>Si</label>
         <label>
         <input type="radio" name="checkFi" id="checkFi1" value="0" onchange="ocultarContentDoc()" <?php if($existeFideo==0){echo "checked";}else{echo "disabled";} ?>>No</label>
         <br>
         <input type="text" style="display: none;" id="esFide" class="form-control" name="fideicomiso" readonly>
        </div>
        <div class='form-group col-md-4'>
          <div class="form-group row">
            <div class="form-group col-md-6">
            <h3>Uso</h3>
            <br/>
            <label>Domestico</label><span>*</span>
            <br/>
            <br/>
            <label>Comercial</label><span>*</span>
            <br/>
            <br/>
            <label>Industrial</label><span>*</span>
            <br/>
            <br/>
            <label for="otros">Otros</label>
            <input type="number" id="otrossed" class="form-control form-control-sm" name="otrossed" readonly>
          </div>
          <div class="form-group col-md-6">
            <h3>Unidades</h3>
            <br/>
            <input type="number" class="form-control form-control-sm amt" id="domesed" name="domesed" value="<?php echo $rDomesed; ?>" readonly/>
            <br/>
            <input type="number" class="form-control form-control-sm amt" id="comered" name="comered" value="<?php echo $rComered; ?>" readonly/>
            <br/>
            <input type="number" class="form-control form-control-sm amt" id="inded" name="inded" value="<?php echo $rIndused; ?>" readonly/>   
            <br>
            <br>
            <input type="number" class="form-control form-control-sm amt" id="otrosed" name="otrosed" value="<?php echo $rmixed; ?>" readonly> 
          </div>
          <label>Total de unidades</label>
            <input type="number" id="totunied" name="totunied" class="form-control" value="<?php echo $rToted; ?>" readonly />
          </div>      
         </div>

         <div class='form-group col-md-12'>
          
         <br>
          <div id="contentCo" >
           <table class="table table-hover tablaCo">
            <thead>
              <tr>
                 <td>RFC</td>
                 <td>Nombre o Razon social</td>
                 <td>Calle</td>
                 <td>No. ext</td>
                 <td>No. int</td>
                 <td>Colonia</td>
                 <td>Municipio</td>
                 <td></td>

              </tr>
             </thead>
             <tbody id="bodyCoEd">
             	<?php $str_queryCo="SELECT *
							FROM SCG.dbo.Copropietario where Expediente = '".$expediente."'";
	
						$queryCo = sqlsrv_query($conn, $str_queryCo);
						while($copropietarioE=sqlsrv_fetch_array($queryCo)){ 
							$idMuniCO=$copropietarioE['Municipio'];
						?>
             	<tr>
        			<td><input type="text" name="rfcCo[]" class="form-control" value="<?php echo $copropietarioE['RFC'];?>" readonly/></td>
        			<td><input type="text" name="nombreCo[]" class="form-control" value="<?php echo $copropietarioE['Cliente'];?>" readonly/></td>
        			<td><input type="text" name="calleCo[]" class="form-control" value="<?php echo $copropietarioE['Calle'];?>" readonly/></td>
        			<td><input type="text" name="noextCo[]" class="form-control" value="<?php echo $copropietarioE['Noext'];?>" readonly/></td>
        			<td><input type="text" name="nointCo[]" class="form-control" value="<?php echo $copropietarioE['Noint'];?>" readonly/></td>
        			<td><input type="text" name="coloniaCo[]" class="form-control" value="<?php echo $copropietarioE['Colonia'];?>" readonly/></td>
        			<td><select name="municipioCo[]" class="form-control" readonly disabled>
        				<option>- - -</option>
        				 <?php
            				$queryMunCO="SELECT idMunicipios,Nombre from Municipios where IdEstado=22";
            				$resultadoMuCO=sqlsrv_query($conn,$queryMunCO);
            
           	 				while($filaMUCO = sqlsrv_fetch_array($resultadoMuCO))
           						{
           	 						$selectedMC= ($idMuniCO == $filaMUCO['idMunicipios'])?"selected":"";
            			?>	
        					 <option value=" <?php echo $filaMUCO['idMunicipios']?>" <?php echo $selectedMC ?> ><?php echo $filaMUCO['Nombre']?></option>
            			<?php 
            				}
           				?>
        				</select></td>
        			<td><button type="button" name="remove" class="remove btn btn-danger">-</button></td>
        		</tr>
        		<?php } ?>
             </tbody>
        </table> 
        <input id="agregartablaCo" class="add btn btn-primary" type="button" value="+" /> 
      </td>            
    </tr>
   </table>
         </div>
         </div>
    <!--representante legal-->
        <div class='form-group col-md-11'>
        <h2>Representante Legal</h2>
        <h4>Escriba los siguientes datos del representante legal</h4>
        <label>¿Se promueve y representa en propio derecho?</label><span>*</span>
        <br>
         <label><input type="radio" id="pregReped" name="pregReped" value="1" <?php if($srep==1) {echo "checked";}else{echo "disabled";} ?>> Si</label>
         <label><input type="radio" id="pregRepNed" name="pregReped" value="0" <?php if($srep==0) {echo "checked";}else{echo "disabled";} ?>>No</label>
         <br>
         <label>Tipo de Representante</label>
          <select id="tipRep" class="form-control rep" name="tipRep" readonly>
           <option value=""></option>
          <?php 
            $queryRepr="SELECT idTiporepresentante,DescripciondelTipo from Tiporepresentante";
            $resultadoRepr=sqlsrv_query($conn,$queryRepr);  
            while($filaRepr = sqlsrv_fetch_array($resultadoRepr))
           {
           	$selectedRepr= ($tipRep == $filaRepr['idTiporepresentante'])?"selected":"";
            ?>
             <option value=" <?php echo $filaRepr['idTiporepresentante']?>" <?php echo $selectedRepr ?>><?php echo $filaRepr['DescripciondelTipo']?></option>
            <?php 
            }
           ?>
         </select>
         <label>No. Documento</label><span>*</span>
         <input type="text" id="numDoced" name="numDoced" class="form-control rep" value="<?php echo $nDoc; ?>" readonly>
         <label>Tipo de Documento(Acredite personalidad)</label><span>*</span>
          <select id="tipdoc" name="tipdoc" class="form-control rep" readonly>
            <option value="0" selected="selected" disabled="disabled">Seleccona el tipo de documento</option>
             <?php
            $queryTi="SELECT * FROM Tipodocumentos WHERE Categoria=53 and Tipotramite=1 and Tipomovimiento=7 ORDER BY Documento";
            $resultadoTi=sqlsrv_query($conn,$queryTi);    
            while($filaT = sqlsrv_fetch_array($resultadoTi)) 	
           	{
           		$selectedT= ($tDoc == $filaT['idTipodocumentos'])?"selected":"";
            ?>
             <option value=" <?php echo $filaT['idTipodocumentos']?>" <?php echo $selectedT; ?>><?php echo $filaT['Documento']?></option>
            <?php 
            }
           ?>
          </select>
          <br>
    <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col"><label>Nombre:</label><span>*</span></th>
      <th scope="col"><label>Telefono:</label><span>*</span></th>
      <th scope="col"><label>Correo:</label><span>*</span></th>
      <th hidden="hidden"><label>Seleccionado:</label></th>
    </tr>
  </thead>
  <tbody>
    <tr> 
      <th><input type='text' id='nombreed' name='nombreed' class='form-control' value="<?php echo $nombre; ?>" readonly/></th>
      <td><input type='text' id='telefono' name='telefonoed' class='form-control' maxlength="10" value="<?php echo $telRep; ?>" readonly/></td>
      <td><input type='email' id='correoREP' name='correoREPed' class='form-control entrada-usuario' value="<?php echo $correorep; ?>" readonly/></td>
      <td hidden="hidden"><label><input type="radio" id="sir" name="seleccionado" value="1" checked="checked"> Si</label>
           <label><input type="radio" id="nor" name="seleccionado" value="0"> No</label>
      </td>
    </tr>
   </tbody>
 </table>
</div>

<br>
<!--Documentos-->
<div class='form-group col-md-12' id="tablaDoced">
  <h3><strong>Documentos del Predio</strong></h3>
   <br><!--aqui va la tabla de documentos.-->   
  <div id="tablaDoced">
  <br>
<p>Favor de colocar solo archivos con extension .pdf</p>
  <table id="tablaDoced11" class="table table-hover" >
  <thead>
    <tr>
      <th scope="col">Documento</th>
      <th scope="col">Anexo</th>
    </tr>
  </thead>
  <tbody>
    <!--1er documento-->
    <tr>
     <th><label>Solicitud para trámite de factibilidad de servicios</label></th>
      <td><input type="file" id="filePickerDed" name="archivoDed" class="form-control inputfile inputfile-7" accept=".pdf" >
         <label for="filePickerDed"> <span id="ared">Cargar Archivo</span></label>
        <br>
        <input type="hidden" id='base64textareaDed' name='codeDed' class='form-control' /></td>
    </tr>
    <tr>
      <th><label>Informe Uso Suelo</label></th>
        <td><input type="file" id="filePickerD1ed" name="archivoD1ed" class="form-control inputfile inputfile-7" accept=".pdf">
        <label for="filePickerD1ed"><span id="ar1ed">Cargar Archivo</span></label>
        <br>
        <input type='hidden' id='base64textareaD1ed' name='codeD1ed' class='form-control' /></td>
    </tr>
    <!--2do documento-->
    <tr>
     <th><label>Archivo polígono KML</label></th>
      <td>
        <input type="file" id="filePickerD2ed" name="archivoD2ed" class="form-control inputfile inputfile-7" accept=".kml,.kmz">
        <label for="filePickerD2ed"><span id="ar2ed">Cargar Archivo</span></label>
        <br>
        <input type='hidden' id='base64textareaD2ed' name='codeD2ed' class='form-control' /></td>
    </tr>
  <!--3er documento-->
    <tr>
     <th><label>Croquis predio plan parcial desarrollo</label></th>
      <td><input type="file" id="filePickerD3ed" name="archivoD3ed" class="form-control inputfile inputfile-7" accept=".pdf" >
        <label for="filePickerD3ed"><span id="ar3ed">Cargar Archivo</span></label>
        <br>
        <input type='hidden' id='base64textareaD3ed' name='codeD3ed' class='form-control' /></td>
    </tr>
      
      <!--4to documento-->
      <tr>
    <th><label>Copia certificada de la escritura de propiedad del predio</label></th>
        <td>
          <input type="file" id="filePickerD4ed" name="archivoD4ed" class="form-control inputfile inputfile-7" accept=".pdf" >
          <label for="filePickerD4ed"><span id="ar4ed">Cargar Archivo</span></label>
          <br>
          <input type='hidden' id='base64textareaD4ed' name='codeD4ed' class='form-control'/></td>
    </tr>

       <!--5to documento-->
      <tr>
     <th><label>Croquis referencia de calles (Google mapa color)</label></th>
        <td>
          <input type="file" id="filePickerD5ed" name="archivoD5ed" class="form-control inputfile inputfile-7" accept=".pdf" >
          <label for="filePickerD5ed"><span id="ar5ed">Cargar Archivo</span></label>
          <br/>
          <input type='hidden' id='base64textareaD5ed' name='codeD5ed' class='form-control'/>
          </td>
      </tr>
  </tbody>
</table>

      </div>          
      </div>

        </div>
    </div>
      <div class='modal-footer'>
        
      <div id='precarga'></div>
       <input type='button' id='btn_enviar_editar' name='creaCaso' value='Enviar Solicitud' class='btn btn-primary' onclick="realizaProcesoEditar()" />
      </div>
    </div>
  </div>
 </div>
</form>


<script>
  $(document).ready(function(){

$( '.inputfile' ).each( function()
  {
    let $input   = $( this ),
      $label   = $input.next( 'label' ),
      labelVal = $label.html();

    $input.on( 'change', function( e )
    {
      let fileName = '';

      if( this.files && this.files.length > 1 ){
        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
      }
      else if( e.target.value ){
        fileName = e.target.value.split( '\\' ).pop();
      }

      if( fileName ){
        $label.find( 'span' ).html( fileName );

      }
      else{
        $label.html( labelVal );
      }

    });// fin change
  });//fin each
});
</script>
  <script>
          var handleFileSelect = function(evt) {
            var files = evt.target.files;
            var file = files[0];
        
            if (files && file) {
                var reader = new FileReader();
        
                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    document.getElementById("base64textareaDed").value = btoa(binaryString);
                };
        
                reader.readAsBinaryString(file);
            }
        };
        
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById('filePickerDed').addEventListener('change', handleFileSelect, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }
    </script>   

<script>
    var handleFileSelect = function(evt) {
      var files = evt.target.files;
      var file = files[0];
  
      if (files && file) {
          var reader = new FileReader();
  
          reader.onload = function(readerEvt) {
              var binaryString = readerEvt.target.result;
              document.getElementById("base64textareaD1ed").value = btoa(binaryString);
          };
          reader.readAsBinaryString(file);
      }
  };
  
  if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('filePickerD1ed').addEventListener('change', handleFileSelect, false);
  } else {
      alert('The File APIs are not fully supported in this browser.');
  }
  </script>   

<script>
      var handleFileSelect = function(evt) {
      var files = evt.target.files;
      var file = files[0];
  
      if (files && file) {
          var reader = new FileReader();
  
          reader.onload = function(readerEvt) {
              var binaryString = readerEvt.target.result;
              document.getElementById("base64textareaD2ed").value = btoa(binaryString);
          };
  
          reader.readAsBinaryString(file);
      }
  };
  
  if (window.File && window.FileReader && window.FileList && window.Blob) {
      document.getElementById('filePickerD2ed').addEventListener('change', handleFileSelect, false);
  } else {
      alert('The File APIs are not fully supported in this browser.');
  }
  </script>  
  <script>
      var handleFileSelect = function(evt) {
        var files = evt.target.files;
        var file = files[0];    
        if (files && file) {
            var reader = new FileReader();
    
            reader.onload = function(readerEvt) {
                var binaryString = readerEvt.target.result;
                document.getElementById("base64textareaD3ed").value = btoa(binaryString);
            };

            reader.readAsBinaryString(file);
        }
    };
    
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        document.getElementById('filePickerD3ed').addEventListener('change', handleFileSelect, false);
    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
    </script>
    <script>
      var handleFileSelect = function(evt) {
        var files = evt.target.files;
        var file = files[0];    
        if (files && file) {
            var reader = new FileReader();
    
            reader.onload = function(readerEvt) {
                var binaryString = readerEvt.target.result;
                document.getElementById("base64textareaD4ed").value = btoa(binaryString);
            };

            reader.readAsBinaryString(file);
        }
    };
    
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        document.getElementById('filePickerD4ed').addEventListener('change', handleFileSelect, false);
    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
    </script>
        <script>
      var handleFileSelect = function(evt) {
        var files = evt.target.files;
        var file = files[0];    
        if (files && file) {
            var reader = new FileReader();
    
            reader.onload = function(readerEvt) {
                var binaryString = readerEvt.target.result;
                document.getElementById("base64textareaD5ed").value = btoa(binaryString);
            };

            reader.readAsBinaryString(file);
        }
    };
    
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        document.getElementById('filePickerD5ed').addEventListener('change', handleFileSelect, false);
    } else {
        alert('The File APIs are not fully supported in this browser.');
    }
    </script>
