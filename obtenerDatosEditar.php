<?php 
require_once 'conexion.php';
require_once 'conexionEditar.php';

function listatipoVivienda($id)
{
$conn=conexion();
$sqlTviv = "select * from Tipovivienda";
$sqltvivR = sqlsrv_query($conn,$sqlTviv);
$lista = "<select id='tipvived' name='tipvived_slc' class='form-control' required>";
$lista .= "<option value=''>- - -</option>";
 while($fila = sqlsrv_fetch_array($sqltvivR)){
   
    $selected= ($id == $fila['idTipovivienda'])?"selected":"";

    $lista .= sprintf(
        "<option value='%d' $selected>%s</option>",
        $fila['idTipovivienda'],
        $fila['Tipo']
    ); 
 }
 $lista.="</select>";

 return $lista;

}

function listaRegimenEd($id)
{
$conn=conexion();

$sqlRcons = "SELECT * FROM TipoRegimen";
$sqlrConst = sqlsrv_query($conn,$sqlRcons);
$lista = "<select id='regConsed' name='regConsed_slc' class='form-control' required>";
$lista .= "<option value=''>- - -</option>";
 while($fila  = sqlsrv_fetch_array($sqlrConst)){
   
    $selected= ($id == $fila['idTipoRegimen'])?"selected":"";

    $lista .= sprintf(
        "<option value='%d' $selected>%s</option>",
        $fila['idTipoRegimen'],
        $fila['Regimenconstruccion']
    ); 
 }
 $lista.="</select>";

 return $lista;

}


function listaMunicipiosEditar($id)
{
$conn=conexion();

$sqlMun="SELECT * FROM Municipios WHERE IdEstado=22";
$sqlRmun = sqlsrv_query($conn,$sqlMun);
$lista = "<select id='muned' name='muned_slc' class='form-control' required>";
$lista .= "<option value=''>- - -</option>";
 while($fila  = sqlsrv_fetch_array($sqlRmun)){
   
    $selected= ($id == $fila['idMunicipios'])?"selected":"";

    $lista .= sprintf(
        "<option value='%d' $selected>%s</option>",
        $fila['idMunicipios'],
        $fila['Nombre']
    ); 
 }
 $lista.="</select>";

 return $lista;

}

function listaTipoRepreseEditar($id)
{
$conn=conexion();

$sqlTR = "SELECT * FROM Tiporepresentante";
$sqlqTR = sqlsrv_query($conn,$sqlTR);
$lista = "<select id='tipReped' name='tipReped_slc' class='form-control rep' required>";
$lista .= "<option value=''>- - -</option>";
 while($fila  = sqlsrv_fetch_array($sqlqTR)){
   
    $selected= ($id == $fila['idTiporepresentante'])?"selected":"";

    $lista .= sprintf(
        "<option value='%d' $selected>%s</option>",
        $fila['idTiporepresentante'],
        $fila['DescripciondelTipo']
    ); 
 }
 $lista.="</select>";

 return $lista;

}


function listaTipoDocuEditar($id)
{
$conn=conexion();

$sqlTD = "SELECT * FROM Tipodocumentos WHERE Categoria=53 and Tipotramite=1 and Tipomovimiento=7 ORDER BY Documento";
$sqlqTD = sqlsrv_query($conn,$sqlTD);
$lista = "<select id='tipdoced' name='tipdoced_slc' class='form-control rep' required>";
$lista .= "<option value=''>- - -</option>";
 while($fila  = sqlsrv_fetch_array($sqlqTD)){
   
    $selected= ($id == $fila['idTipodocumentos'])?"selected":"";

    $lista .= sprintf(
        "<option value='%d' $selected>%s</option>",
        $fila['idTipodocumentos'],
        $fila['Documento']
    ); 
 }
 $lista.="</select>";

 return $lista;

}

$caso=$_POST['selectIdCase'];
$folio=$_POST['selectRadNumber'];

$consulta="select top 1 sp.Folio,sp.Usuario,sp.Tipotramite,Tipomovimiento,sp.Correo,sp.Fecha,sp.Serepresenta,
sp.TipoRepresentante,ex.Desarrollo,ex.Cvecatastral,ex.Superficie,ex.Tipovivienda,ex.Regimenconstruccion,ex.Domicilio,ex.Colonia,
ex.Municipio,ex.CP,ex.Telefono,ex.Correo as correopr,ex.Reqdomestico,ex.Reqcomercial,ex.Reqindustrial,ex.Reqmixto,ex.Totalunidades,
c.Nodocumento,c.Tipodocumento,r.Nombre,r.Telefono as telefonorep,r.Correo as correorep,r.Seleccionado
from Expediente ex 
inner join SolicitudPadron sp on ex.SolicitudPadron=sp.idSolicitudPadron
inner join Cedula c on ex.Cedula=c.idCedula
inner join Representantes r on ex.idExpediente=r.Expediente
where sp.Folio='$folio'";

$sql=sqlsrv_query($conn,$consulta);
$values = sqlsrv_fetch_array($sql);

$folioed=$values['Folio'];
$fechaed=$values['Fecha']->format('Y-m-d');
$correoed=$values['Correo'];
$fracc=$values['Desarrollo'];
$clvCated=$values['Cvecatastral'];
$tipoV=$values['Tipovivienda'];
$cped=$values['CP'];
$teled=$values['Telefono'];
$correopred=$values['correopr'];
$suped=$values['Superficie'];
$rCons=$values['Regimenconstruccion'];
$domi=$values['Domicilio'];
$col=$values['Colonia'];
$mun=$values['Municipio'];
$rDomes=$values['Reqdomestico'];
$rComer=$values['Reqcomercial'];
$rIndus=$values['Reqindustrial'];
$rmix=$values['Reqmixto'];
$rTot=$values['Totalunidades'];
$srep=$values['Serepresenta'];
$tipRep=$values['TipoRepresentante'];
$nDoc=$values['Nodocumento'];
$tDoc=$values['Tipodocumento'];
$nombre=$values['Nombre'];
$telRep=$values['telefonorep'];
$correorep=$values['correorep'];
$sele=$values['Seleccionado'];


$sqlObs=" select ob.Observacion from Expediente ex 
  inner join SolicitudPadron sp on sp.idSolicitudPadron = ex.SolicitudPadron
  inner join Cedula c on c.idCedula = ex.Cedula 
  inner join Observaciones ob on ob.Expediente = ex.idExpediente
  where sp.Folio = '$folio'";
  
  $sql1=sqlsrv_query($conn,$sqlObs);
  $result = sqlsrv_fetch_array($sql1);
 

  $obseva=$result['Observacion'];


 $form="<h4>Coloca el correo </h4>";
    $form.="<form id='edita-caso' name='miformulario' method='POST' enctype='multipart/form-data'>";
      $form.="<div class='row'>";
      $form.="<div class='form-group col-md-4'>";
      $form.="<label for='radNumber'>Folio:</label>";
      $form.="<input type='text' id='radNumbered' name='radNumbered' class='form-control' value='$folioed' readonly='readonly' disabled />";
      $form.="</div>";
      $form.="<div class='form-group col-md-4'>";
        	$form.="<label for='fechaed'>Fecha:</label>";
        	$form.="<input type='text' name='fechaed' class='form-control' value='$fechaed' readonly='readonly' disabled/>";
        $form.="</div>";
      $form.="</div>";
      $form.="<div class='form-group col-md-6'>";
		$form.="<label>Observaciones</label>";
		$form.="<textarea name='observa' class='form-control' readonly='readonly' disabled>$obseva</textarea>";
	  $form.="</div>";
      $form.="<div class='form-group col-md-10'>";
          $form.="<label>Correo electronico para notificar:</label>";
          $form.="<input type='email' id='inEmailed' name='correoed' class='form-control correo' value='$correoed'>";             
          $form.="</div>";
          $form.="<div class='form-group col-md-10'>";
             $form.="<div class='form-group row'>";
             $form.="<div class='form-group col-md-8'>";
                $form.="<h4><strong>Datos del predio</strong></h4>"; 
                $form.="</div>";
                $form.="<div class='form-group col-md-4'>";
                  $form.="<h4><strong>Requerimiento Agua</strong></h4>"; 
                //  $form.="</div>";
              	//  $form.="</div>";
         $form.="</div>";
         $form.="<div class='form-group col-md-4'>";
         $form.="<label>Nombre del Fraccionamiento</label>";
         $form.="<input type='text' id='nomfracced' name='nomFracced' class='form-control' value='$fracc' />";
         $form.="<label>Clave Catastral:</label>";
         $form.="<input type='text' id='clvCated' name='clvCated' class='form-control' value='$clvCated' />";
         $form.="<label>Tipo Vivienda</label>";
         $form.=listatipoVivienda($tipoV);
         // $form.="<input type='text' id='tipvived' class='form-control' name='tipvived' value='$tipoViv' readonly>";
         $form.="<label>C.P</label>";
         $form.="<input type='text' id='cped' name='cped' class='form-control' value='$cped' />";
         $form.="<label>Telefono:</label>";
         $form.="<input type='tel' id='teleed' name='teleed' class='form-control' value='$teled' />";
         $form.="<label>Correo:</label>";
         $form.="<input type='email' class='form-control' id='correopred' name='correopred' value='$correopred' />";
         $form.="</div>";
         $form.="<div class='form-group col-md-3'>";
         $form.="<label>Superficie</label>"; 
         $form.="<input type='number' id='suped' name='suped' class='form-control' value='$suped' />";
         $form.="<label>Regimen Construcción</label>"; 
         $form.=listaRegimenEd($rCons);
         $form.="<label>Domicilio:</label>"; 
         $form.="<input type='text' id='domied' name='domied' class='form-control' value='$domi' />";
         $form.="<label>Colonia:</label>"; 
         $form.="<input type='text' id='coled' name='coled' class='form-control' value='$col' />";
         $form.="<label>Municipio:</label>"; 
         $form.=listaMunicipiosEditar($mun);
         //$form.="<label>Tienes otras Claves?</label>";
          //$form.="<label><input type='radio' id='sic' name='pregunta' value='1'> Si</label>";
          //$form.="<label><input type='radio' id='noc' name='pregunta' value='0'> No</label>";
        $form.="</div>";
        $form.="<div class='form-group col-md-4'>";
          $form.="<div class='form-group row'>";
            $form.="<div class='form-group col-md-6'>";
            $form.="<h3>Uso</h3>";
            $form.="<br/>";
            $form.="<label>Domestico</label>";
            $form.="<br/>";
            $form.="<br/>";
            $form.="<label>Comercial</label>";
            $form.="<br/>";
            $form.="<br/>";
            $form.="<label>Industrial</label>";
            $form.="<br/>";
            $form.="<br/>";
            $form.="<label for='otros'>Otros</label>";
            $form.="<input type='text' id='otrossed' class='form-control form-control-sm' name='otrossed' />";
          $form.="</div>";
          $form.="<div class='form-group col-md-6'>";
            $form.="<h3>Unidades</h3>";
            $form.="<br/>";
            $form.="<input type='text' class='form-control form-control-sm amt' id='domesed' name='domesed' value='$rDomes' />";
            $form.="<br/>";
            $form.="<input type='text' class='form-control form-control-sm amt' id='comered' name='comered' value='$rComer' />";
            $form.="<br/>";
            $form.="<input type='text' class='form-control form-control-sm amt' id='inded' name='inded' value='$rIndus' />";   
            $form.="<br>";
            $form.="<br>";
            $form.="<input type='text' class='form-control form-control-sm amt' id='otrosed' name='otrosed' value='$rmix' />"; 
          $form.="</div>";
          $form.="<label>Total de unidades</label>";
            $form.="<input type='number' id='totunied' name='totunied' class='form-control' value='$rTot' readonly/>";
          $form.="</div>";
         $form.="</div>";
        $form.="<div class='form-group col-md-11'>";
        $form.="<h2>Representante Legal</h2>";
        $form.="<h4>Escriba los siguientes datos del representante legal</h4>";
        $form.="<label>¿Se promueve y representa en propio derecho? </label>";
        $form.="<br>";
         $form.="<label><input type='radio' id='sired' name='pregReped' value='1'  if( $srep == 1 ) { checked='checked' } readonly> Si</label>";
         $form.="<label><input type='radio' id='nored' name='pregReped' value='0'  if($srep==0)?checked='checked':''; readonly> No</label>";
         $form.="<br>";
         $form.="<label>Tipo de Representante</label>";
          $form.=listaTipoRepreseEditar($tipRep);
          $form.="<label>No. Documento</label>";
         $form.="<input type='text' id='numDoced' name='numDoced' class='form-control rep' value='$nDoc' >";
         $form.="<label>Tipo de Documento</label>";
         $form.=listaTipoDocuEditar($tDoc);
         $form.="<br>";
    $form.="<table class='table table-hover'>";
  $form.="<thead>";
    $form.="<tr>";
      $form.="<th scope='col'><label>Nombre:</label></th>";
      $form.="<th scope='col'><label>Telefono:</label></th>";
      $form.="<th scope='col'><label>Correo:</label></th>";
     // $form.="<th scope='col'><label>Seleccionado:</label></th>";
    $form.="</tr>";
  $form.="</thead>";
  $form.="<tbody>";
    $form.="<tr>";
      $form.="<th scope='row'><input type='text' id='nombreed' name='nombreed' class='form-control' value='$nombre' /></th>";
      $form.="<td><input type='text' id='telefonoed' name='telefonoed' class='form-control' value='$telRep' /></td>";
      $form.="<td><input type='email' id='correoREPed' name='correoREPed' class='form-control entrada-usuario' value='$correorep' /></td>";
      //$form.="<td> <label><input type='radio' id='sired' name='seleccionadoed' value='1' if($sele == 1) { checked='checked' }else{} > Si</label>
        //   <label><input type='radio' id='nored' name='seleccionadoed' value='0' >No</label></td>";
    $form.="</tr>";
   $form.="</tbody>";
 $form.="</table>";
$form.="</div>";

$form.="<h4>Documentos del Predio</h4>";
            

printf($form);
 ?>