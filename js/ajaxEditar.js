function realizaProcesoEditar(){
    var url = "llamarWebServiceEditar.php"; // El script a dónde se realizará la petición.
    /*var numCaso=$("#numCaso").val();
    var numRadNumber=$("#numRadNumbered").val();
    var rfc=$("#rfc").val();


    var correoed=$("#inEmailed").val();
    var fracced=$("#nomfracced").val();
    var claveed=$("#clvCat").val();
    if(claveed == ''){claveed="NULL";}
    var tipViv=$("#tipviv").val();
    var cped=$("#cped").val();
    if(cped == ''){cped="NULL";} 
    var teleed=$("#teleed").val();
    if(teleed == ''){teleed="NULL";}
    var correopred=$("#correopred").val();
    if(correopred== ''){correopred="NULL";}
    var superfed=$("#suped").val();
    var regConsed=$("#regconsed").val();
    if(regConsed == ''){regConsed="NULL";}
    var domed=$("#domied").val();
    var coloned=$("#coled").val();
    var muned=$("#muned").val();
    //var pregunta= $("input[name='pregunta']:checked"). val();
    //if(pregunta){}else{radioValue="NULL";} 
    var usootrosed=$("#otrossed").val();
    if (usootrosed== ''){usootrosed="NULL";}
    var usodomesed=$("#domesed").val();
    var usocomered=$("#comered").val();
    var usoinded=$("#inded").val();
    var otroed=$("#otrosed").val();
    var undtoted=$("#totunied").val();
    var radioValue2= $("input[name='pregRep']:checked"). val();
    if(radioValue2){}else{radioValue2="NULL";}
    var tipReped=$("#tipReped").val();
    if(tipReped == ''){tipReped="NULL";}
    var numdoced=$("#numDoced").val();
    var tipodoced=$("#tipdoced").val();
    var nomReped=$("#nombreed").val();
    var telefonoReped=$("#telefonoed").val();
    var correoReped=$("#correoREPed").val();

    var edfichero=$("#ficheroDed").val();
    var code1=$("#base64textareaDed").val();
    var edfichero2=$("#ficheroD1ed").val();
    var code2=$("#base64textareaD1ed").val();
    var edfichero3=$("#ficheroD2ed").val();
    var code3=$("#base64textareaD2ed").val();
    var edfichero4=$("#ficheroD3ed").val();
    var code4=$("#base64textareaD3ed").val();


    var param={
                 'numCaso':numCaso,
                 'numRadNumber':numRadNumber,
                 'rfc':rfc,
                 'correoed':correoed,
    			 'fracced':fracced,
   				 'claveed':claveed,
    			 'cped':cped,
                 'teleed':teleed,
    		     'correopred':correopred,
                 'superfed':superfed,
                 'domed':domed,
                 'coloned':coloned,
                 'muned':muned,
                 'usootrosed':usootrosed,
                 'usodomesed':usodomesed,
                 'usocomered':usocomered,
                 'usoinded':usoinded,
                 'otroed':otroed,
                 'undtoted':undtoted,
                 'tipReped':tipReped,
                 'numdoced':numdoced,
                 
                 'nomReped':nomReped,
                 'telefonoReped':telefonoReped,
                 'correoReped':correoReped,

                 'edfichero':edfichero,
                 'code1':code1,
                 'edfichero2':edfichero2,
                 'code2':code2,
                 'edfichero3':edfichero3,
                 'code3':code3,
                 'edfichero4':edfichero3,
                 'code4':code3,
    }
   */
const form = document.getElementById('edita-caso');
const paqueteDeDatos = new FormData(form);
   //console.log(form.serialize());
   
      $.ajax({
        
        beforeSend: function(){
             $('#precarga').html('<span>Un momento por favor ...</span>');
        },

            type: "POST",
              url: url,
              contentType: false,
              data: paqueteDeDatos, // Adjuntar los campos del formulario enviado.
              processData: false,
              cache: false, 
              dataType: "html", 
             success: function(data)
             {
                console.log(data);
                $('#precarga').hide(1000);
                //$("#precarga").html(data); // Mostrar la respuestas del script PHP.
                //alert(data);

             },
             complete: function()
             {
                var folio = $('#radNumbered').val();
                swal({
                    title: 'Exito!',
                    text: "Su solicitud con el folio:"+folio+" Ha sido modificada y enviada.",
                    type: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!'
                  }).then((result) => {
                    if (result.value) {
                      window.location.reload();
                    }
                  })
              
               
             }         
                //$("#respuesta").hide(3500);
           
            
             
           });
           //
           
        return false; // Evitar ejecutar el submit del formulario
}

function showContented(){
        element = document.getElementById("contented");
        check = document.getElementById("checked");
        if (check.checked) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
        }
    
}

function ocultarContented(){
        element = document.getElementById("contented");
        check = document.getElementById("checked1");
        if (check.checked) {
            element.style.display='none';
        }   
}


$(document).on('click','.editar',function(){
    var edit_id=$(this).attr('id');
    $.ajax({
        url:"edit_data.php",
        type:"post",
        data:{edit_id:edit_id},
        success:function(data){
            $("#info-update").html(data);
            $(".modal-header").css('background-color','#3393FF'); 
            $(".modal-header").css('color','white'); 
            $(".modal-title").text(" Editar Nueva Solicitud de Factibilidad");
            $("#editData").modal('show');
        }
    });

});