$(document).ready(function(){

$( '.inputfile' ).each( function()
  {
    let $input   = $( this ),
      $label   = $input.next( 'label' ),
      labelVal = $label.html();

    $input.on( 'change', function( e )
    {
      let fileName = '';

      if( this.files && this.files.length > 1 ){
        fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
      }
      else if( e.target.value ){
        fileName = e.target.value.split( '\\' ).pop();
      }

      if( fileName ){
        $label.find( 'span' ).html( fileName );

      }
      else{
        $label.html( labelVal );
      }

    });// fin change
  });//fin each



$("input").focus(function(){

    ndocRepres = $("#numDoc").val(),
    tipDocumento = $("#tipdoc").val(),
    nomRepres = $("#nombre").val(),
    telRepres = $("#telefono").val(),
    coRepres = $("#correoREP").val();
    $('.alert').remove();
    colorDefault('inEmail');
    colorDefault('nomfracc');
    colorDefault('clvCat');
    colorDefault('sup');
    colorDefault('domi');
    colorDefault('col');
    colorDefault('domes');
    colorDefault('comer');
    colorDefault('ind');
    colorDefault('numDoc');
    colorDefault('nombre');
    colorDefault('telefono');
    colorDefault('correoREP');
    colorDefault('filePickerD');
    colorDefault('filePickerD1');
    colorDefault('filePickerD2');
    colorDefault('filePickerD3');
    colorDefault('filePickerD4');
    colorDefault('filePickerD5');
  
});
$("select").focus(function(){
  $('.alert').remove();
  colorDefault('regcons');
  colorDefault('tipviv');
  colorDefault('mun');
  colorDefault('tipRep');
  colorDefault('tipdoc');
});
    tablaCasos=$("#tblCasos").DataTable({
        //"columnDefs":[{
          //  "targets":-1,
           // "data":null,
            //"defaultContent":"<button type='button' class='editar btn btn-primary'><i class='fa fa-pencil'></i></button>"
        //}],
        "language": {
    "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            }
    });
var cont=0;
function Agregar(){
  //alert("text");
  cont++;
  var fila='<tr><th scope="row"><select id="doc" name="doc" class="form-control"><option value="">Selecciona el tipo</option></select></th><td>hola</td></tr>';
  $("#tabla").append(fila);
}

$("#btn-add").click(function(){
  Agregar();
});
$("#btnNuevo").click(function(){
  $("#alta-caso").trigger("reset");
  //creamos el caso para que aparezca el VUD
     // alert("aqui crearemos el caso");
      var url = "crearCaso.php"; // El script a dónde se realizará la petición.
      var rfc = $('#folio').val();
      $.ajax({
        type: "POST",
        url:url,
        data: {"rfc":rfc},
        success: function(r)
            {
              console.log(r);
              $('#radNumber').val(r);
                
               
            },
    });
  
   
   $(".modal-header").css('background-color','#3393FF'); 
   $(".modal-header").css('color','white'); 
   $(".modal-title").text("Nueva Solicitud de Factibilidad");
   $("#btn_creaPDF").attr("disabled", false);
   //$("#addModal").modal('show');
   $('#addModal').modal({ backdrop: 'static', keyboard: false });
    $('#addModal').modal('show');
});//se cierra el click de btnNuevo.


// $(".editar").click(function(){
  // $(".modal-header").css('background-color','#3393FF'); 
   //$(".modal-header").css('color','white'); 
   //$(".modal-title").text(" Editar Nueva Solicitud de Factibilidad");
   // fila = $(this).closest("tr");
   
   // id = parseInt(fila.find('td:eq(0)').text());
   // radnumber = fila.find('td:eq(1)').text();
    
   // var midVal = $('#numCaso').val(id);
   // var midValRad = $('#numRadNumber').val(radnumber);
    
     //obtenerForm();
    //$("#editarModal").modal('show');
   
 //});//se cierra el click de editar

 
   $('.amt').keyup(function() {
    var importe_total = 0
        $(".amt").each(
            function(index, value) {
                if ( $.isNumeric( $(this).val() ) ){
                    importe_total = importe_total + eval($(this).val());
                    //console.log(importe_total);
                }
            }
        );//se cierra el each
        $("#totuni").val(importe_total);
    });//se cierra el keyup

});//se cierra el document




 function obtenerForm(){
        selectRadNumber= $("#numRadNumber").val();
        selectIdCase=$("#numCaso").val();

     //alert(selectIdCase);
    $.ajax({
        type: "POST",
        url:"obtenerDatosEditar.php",
        data: {"selectRadNumber":selectRadNumber,"selectIdCase":selectIdCase},
        success: function(r)
            {
                
                $("#editarForm").html(r);
               
            },
    });
 }


// function conviertePDF(){

// var  fol= $('#radNumber').val();
                
//               $("#btn_enviar").css("display", "block"); 
//               $("#guardar").css("display", "none");
//                 $.ajax({
//                   url: 'base64.php',
//                   type: 'post',
//                   data: {'fol': fol},
//                 })
//                 .done(function(data) {
//                     $("#base64textareaPDF").val(data); 
//                     //console.log(data);
//                 })
          


            
           
// }




function showContent(){
        element = document.getElementById("content");
        check = document.getElementById("check");
        if (check.checked) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
        }
    
}

function showContentDoc(){
        element = document.getElementById("esFide");
        check = document.getElementById("checkFi");
        if (check.checked) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
        }
    
    showContentDoc1();
}

function showContentDoc1(){
        element = document.getElementById("sexto");
        check = document.getElementById("checkdoc");
        if (check.checked) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
        }
    
}

function mostrarCop(){
    element = document.getElementById("contentCo");
        check = document.getElementById("checkCo");
        if (check.checked) {
            element.style.display='block';
        }
        else {
            element.style.display='none';
        }
    
}

function ocultarCo(){
       element = document.getElementById("contentCo");
        check = document.getElementById("checkCo1");
        if (check.checked) {
            element.style.display='none';
        }
       
}


function ocultarContentDoc(){
       element = document.getElementById("esFide");
        check = document.getElementById("checkFi1");
        if (check.checked) {
            element.style.display='none';
        }
       
}

function ocultarContent(){
        element = document.getElementById("content");
        check = document.getElementById("check1");
        if (check.checked) {
            element.style.display='none';
        }
       
    
}


function AgregarCampo(){
$(".tablados")
.append(
    $('<tbody>')
    .append(
        $('<tr>')
         .append(
             $('<td>')
                .append(
                    $('<input>').attr('type','text').addClass('form-control').attr('name', 'txtclvCat[]').attr('placeholder', 'Escribe tu clave')
                    //$('<input>').attr('type','button').attr('value','-').attr('onclick','remueveCampo')
              
              )
          )
      )
  )

}

$(document).on('change', '#filePickerD', function(event) {
  event.preventDefault();
  let filename = $("#filePickerD").val();
  let btnEnviar = $("#btn_enviar");
        // si se eligio un archivo correcto obtiene la extension para vlidarla
             var extension = filename.replace(/^.*\./, '');               
             //console.log(extension);
             if (extension == filename){
               extension = '';
             }else{
                 extension = extension.toLowerCase();
                 //aqui puedes incluir todas las extensiones que quieres permitir
                 if(extension != 'pdf') {
                    mostarAlerta("Solicitud para trámite de factibilidad de servicios su extención no es valida");
                    btnEnviar.attr('disabled', 'true');

                 }else{
                    btnEnviar.removeAttr('disabled');
                 }
                     

   }
 
});

$(document).on('change', '#filePickerD1', function(event) {
  event.preventDefault();
  let filename = $("#filePickerD1").val();
  let btnEnviar = $("#btn_enviar");
        // si se eligio un archivo correcto obtiene la extension para vlidarla
             var extension = filename.replace(/^.*\./, '');               
             //console.log(extension);
             if (extension == filename){
               extension = '';
             }else{
                 extension = extension.toLowerCase();
                 //aqui puedes incluir todas las extensiones que quieres permitir
                 if(extension != 'pdf') {
                    mostarAlerta("Informe Uso de Suelo su extención no es valida");
                    btnEnviar.attr('disabled', 'true');

                 }else{
                    btnEnviar.removeAttr('disabled');
                 }
                     

   }
 
});

$(document).on('change', '#filePickerD2', function(event) {
  event.preventDefault();
    let filename = $("#filePickerD2").val();
    let btnEnviar = $("#btn_enviar");
        // si se eligio un archivo correcto obtiene la extension para vlidarla
             var extension = filename.replace(/^.*\./, '');               
             //console.log(extension);
             if (extension == filename){
               extension = '';
             }else{
                 extension = extension.toLowerCase();
                 //aqui puedes incluir todas las extensiones que quieres permitir
                 if(extension != 'kmz') {
                    mostarAlerta("Archivo polígono KML su extención no es valida");
                    btnEnviar.attr('disabled', 'true');
                    
                 }else{
                    btnEnviar.removeAttr('disabled');
                 }
   }
});

$(document).on('change', '#filePickerD3', function(event) {
  event.preventDefault();
  let filename = $("#filePickerD3").val();
  let btnEnviar = $("#btn_enviar");
        // si se eligio un archivo correcto obtiene la extension para vlidarla
             var extension = filename.replace(/^.*\./, '');               
             //console.log(extension);
             if (extension == filename){
               extension = '';
             }else{
                 extension = extension.toLowerCase();
                 //aqui puedes incluir todas las extensiones que quieres permitir
                 if(extension != 'pdf') {
                    mostarAlerta("Croquis predio plan parcial desarrollo su extención no es valida");
                    btnEnviar.attr('disabled', 'true');

                 }else{
                    btnEnviar.removeAttr('disabled');
                 }
                     

   }
 
});

$(document).on('change', '#filePickerD4', function(event) {
  event.preventDefault();
  let filename = $("#filePickerD4").val();
  let btnEnviar = $("#btn_enviar");
        // si se eligio un archivo correcto obtiene la extension para vlidarla
             var extension = filename.replace(/^.*\./, '');               
             //console.log(extension);
             if (extension == filename){
               extension = '';
             }else{
                 extension = extension.toLowerCase();
                 //aqui puedes incluir todas las extensiones que quieres permitir
                 if(extension != 'pdf') {
                    mostarAlerta("Copia certificada de la escritura de propiedad del predio su extención no es valida");
                    btnEnviar.attr('disabled', 'true');

                 }else{
                    btnEnviar.removeAttr('disabled');
                 }
                     

   }
 
});

$(document).on('change', '#filePickerD5', function(event) {
  event.preventDefault();
  let filename = $("#filePickerD5").val();
  let btnEnviar = $("#btn_enviar");
        // si se eligio un archivo correcto obtiene la extension para vlidarla
             var extension = filename.replace(/^.*\./, '');               
             //console.log(extension);
             if (extension == filename){
               extension = '';
             }else{
                 extension = extension.toLowerCase();
                 //aqui puedes incluir todas las extensiones que quieres permitir
                 if(extension != 'pdf') {
                    mostarAlerta("Croquis referencia de calles (Google mapa color) su extención no es valida");
                    btnEnviar.attr('disabled', 'true');

                 }else{
                    btnEnviar.removeAttr('disabled');
                 }
                     

   }
 
});
function realizaProceso(){
let correo = $("#inEmail").val(),
    NomFracc = $("#nomfracc").val(),
    claveCat =  $("#clvCat").val(),
    superf =  $("#sup").val(),
    rConst =  $("#regcons").val(),
    domicilio = $("#domi").val(),
    tVivienda = $("#tipviv").val(),
    colonia = $("#col").val(),
    municipio = $("#mun").val(),

    rDomes = $("#domes").val(),
    rcomer = $("#comer").val(),
    rindus = $("#ind").val(),

   
    tRepres = $("#tipRep").val(),
    ndocRepres = $("#numDoc").val(),
    tipDocumento = $("#tipdoc").val(),
    nomRepres = $("#nombre").val(),
    telRepres = $("#telefono").val(),
    coRepres = $("#correoREP").val();
   
    archivo1 = $("#filePickerD")[0].files.length;
    archivo2 = $("#filePickerD1")[0].files.length;
    archivo3 = $("#filePickerD2")[0].files.length;
    archivo4 = $("#filePickerD3")[0].files.length;
    archivo5 = $("#filePickerD4")[0].files.length;
    archivo6 = $("#filePickerD5")[0].files.length;
    
   
    

  $(".alert,alert-warning").remove(); 
  if (correo == "" || correo == null) {
        cambiarColor("inEmail");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor escribe un Correo");
        return false;
  }
  if (NomFracc.trim().length > 0) {
        
  }else {
        cambiarColor("nomfracc");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor escribe el nombre del fraccionamiento");
        return false;
  }
  if (claveCat == "" || claveCat == null) {
        cambiarColor("clvCat");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor escribe tu clave catastral");
        return false;
  }
  if (superf == "" || superf == null) {
        cambiarColor("sup");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor escribe la superficie");
        return false;
  }
  if (rConst == "" || rConst == 0) {
        cambiarColor("regcons");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor selecciona tu Regimen de construcción");
        return false;
  }
  if (domicilio == "" || domicilio == null) {
        cambiarColor("domi");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor escribe tu domicilio");
        return false;
  }
  if (tVivienda == "" || tVivienda == 0) {
        cambiarColor("tipviv");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor selecciona tu tipo de vivienda");
        return false;
  }
  if (colonia == "" || colonia == null) {
        cambiarColor("col");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor escribe tu colonia");
        return false;
  }
  if (municipio == "" || municipio == 0) {
        cambiarColor("mun");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor escribe tu colonia");
        return false;
  }
  if (rDomes == "" || rDomes == null) {
        cambiarColor("domes");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor coloca el numero de unidades domesticas");
        return false;
  }else{
     if (rDomes <= 0) {
        cambiarColor("domes");
        //mostramos el mensaje de alerta
        mostarAlerta("El numero de unidades tiene que ser mayor a cero");
        return false;
     }
  }
  if (rcomer == "" || rcomer == null) {
        cambiarColor("comer");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor coloca el numero de unidades comerciales");
        return false;
  }
  if (rindus == "" || rindus == null) {
        cambiarColor("ind");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor coloca el numero de unidades industriales");
        return false;
  }
  if ($("#alta-caso input[name='pregRep']:radio").is(':checked')) {
   
   }else {
         //mostramos el mensaje de alerta
         mostarAlerta("Por favor selecciona si el representante se promueve en su propio derecho");
         return false;
   }

  if (tRepres == "" || tRepres == 0) {
        cambiarColor("tipRep");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor selecciona el tipo de representante");
        return false;
  }
  if (ndocRepres == "" || ndocRepres == null) {
        cambiarColor("numDoc");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor coloca el numero de documento");
        return false;
  }
  if (tipDocumento == "" || tipDocumento == 0) {
        cambiarColor("tipdoc");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor coloca selecciona el tipo de documento");
        return false;
  }
  if (nomRepres == "" || nomRepres == null) {
        cambiarColor("nombre");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor coloca el nombre del representante");
        return false;
  }
  if (telRepres == "" || telRepres == null) {
        cambiarColor("telefono");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor coloca el numero del representante");
        return false;
  }
  if (coRepres == "" || coRepres == null) {
        cambiarColor("correoREP");
        //mostramos el mensaje de alerta
        mostarAlerta("Por favor coloca el correo del representante");
        return false;
  }  
  if (archivo1 === 0) {
      cambiarColor("filePickerD");
        //mostramos el mensaje de alerta
      mostarAlerta("Por favor coloca la Solicitud para trámite de factibilidad de servicios");
      return false;
  }
  if (archivo2 === 0) {
      cambiarColor("filePickerD1");
        //mostramos el mensaje de alerta
      mostarAlerta("Por favor coloca el Archivo Informe Uso Suelo.");
      return false;
  }
  if (archivo3 === 0) {
      cambiarColor("filePickerD2");
        //mostramos el mensaje de alerta
      mostarAlerta("Por favor coloca el Archivo polígono KML.");
      return false;
  }
  if (archivo4 === 0) {
      cambiarColor("filePickerD3");
        //mostramos el mensaje de alerta
      mostarAlerta("Por favor coloca el Croquis predio plan parcial desarrollo.");
      return false;
  }
  if (archivo5 === 0) {
      cambiarColor("filePickerD4");
        //mostramos el mensaje de alerta
      mostarAlerta("Por favor colocar la Copia certificada de la escritura de propiedad del predio.");
      return false;
  }
  if (archivo6 === 0) {
      cambiarColor("filePickerD5");
        //mostramos el mensaje de alerta
      mostarAlerta("Por favor coloca el Croquis referencia de calles (Google mapa color).");
      return false;
  }
      

const form = document.getElementById('alta-caso');
const paqueteDeDatos = new FormData(form);


var url = "llamarWebService.php"; // El script a dónde se realizará la petición.
        $.ajax({
            beforeSend: function(){
                //$('#precarga').html('<span>Un momento por favor ...</span>');
                $('#precarga').html('<img src="img/cargando.gif" alt="Smiley face" height="42" width="42">');
            },
              type: "POST",
              url: url,
              contentType: false,
              data: paqueteDeDatos, // Adjuntar los campos del formulario enviado.
              processData: false,
              cache: false, 
              dataType: "html", // Adjuntar los campos del formulario enviado.
              success: function(data)
             {
                console.log(data);
                $('#precarga').hide(1000);
                
              
             },
            complete: function(){
                 var folio = $('#radNumber').val();
                swal({
                   title: 'Buen trabajo!',
                    text: "Tu solicitud ha sido enviada con el folio:"+folio,
                    type: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK!',
                    allowOutsideClick: false
                  }).then((result) => {
                    if (result.value) {
                      window.location.reload();
                    }
                  })
              
             }    
             
           });
}



function mostrarTabla() {
$("#tablaDoc1").css("display","block");
}




// creamos un funcion de color por defecto a los bordes de los inputs
function colorDefault(dato){
    $('#' + dato).css({
        border: "1px solid #999"

    });
}

//funcion para cambiar el color de borde
function cambiarColor(dato){
    $('#'+dato).css({
        borderColor:"red"
    });    
}

//funcion para mostrar la alerta
function mostarAlerta(texto){
    $("#vacio").before('<div class="alert alert-warning" role="alert">'+ texto +'</div>');
}




