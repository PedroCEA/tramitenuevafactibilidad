<?php

$radNum=$_GET['radnumber'];
require_once __DIR__ . '/vendor/autoload.php';
//plantilla html
require_once ('solicitudtest.php');
//obtenemos el css
$css=file_get_contents('css/style.css');
//base de datos
require_once 'datosPDF.php';


$mpdf = new \Mpdf\Mpdf();

$plantilla=getPlantilla($datos);
$mpdf->AddPage('P','','','','','8','8','8','8');
$mpdf->WriteHTML($css,\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($plantilla,\Mpdf\HTMLParserMode::HTML_BODY);
$mpdf->Output('Rep_solicitudes/'.$radNum.'.pdf', \Mpdf\Output\Destination::FILE);
$mpdf->Output($radNum.'.pdf','I');